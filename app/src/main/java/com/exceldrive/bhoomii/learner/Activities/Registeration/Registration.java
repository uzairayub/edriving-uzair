package com.exceldrive.bhoomii.learner.Activities.Registeration;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.exceldrive.bhoomii.AppController;
import com.exceldrive.bhoomii.Models.ModelLear.LearLoginResponse;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.SnappyDBUtil;
import com.exceldrive.bhoomii.Utils.Util;
import com.exceldrive.bhoomii.Utils.Utilities;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registration extends AppCompatActivity implements View.OnClickListener {

    private Button btnRegister;
    private Calendar myCalendar = Calendar.getInstance();
    private LearLoginResponse learData;
    private EditText etFullName, etPassword, etConfirmPassword, etEmail, etAddress, etPhone, etZipCode;
    private String fullName, password, dateToStratCourse, confirmPassword, email, address, phone, zipCode;
    private TextView mDateToStratCourse;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        createToken();
        initial();
    }

    private void createToken()
    {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( Registration.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();
            }
        });
    }
    private void initial() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        learData = SnappyDBUtil.getObject(Constants.LEAR_DATA, LearLoginResponse.class);
//        EditText
        etFullName = findViewById(R.id.etFirstName);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        etEmail = findViewById(R.id.etEmail);
        etAddress = findViewById(R.id.etAddress);
        etPhone = findViewById(R.id.etPhone);
        etZipCode = findViewById(R.id.etZipCode);

//        TextView
        mDateToStratCourse = findViewById(R.id.date_to_start_course);
        mDateToStratCourse.setOnClickListener(this);
//        Button
        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if (v.getId() == btnRegister.getId())
        {
            if(validation())
            {
                learData.setLearName(fullName);
                learData.setLearEmail(email);
                learData.setAddress(address);
                learData.setLearPhoneNumber(phone);
                learData.setLearDob(dateToStratCourse);
                learData.setFcmToken(token);
                registerUser();
            }

        }
        else if(v.getId() == mDateToStratCourse.getId())
        {
            mDateToStratCourse.setError(null);
            new DatePickerDialog(Registration.this, dateCallback, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    }

    private boolean validation()
    {
        fullName = etFullName.getText().toString().trim();
        password = etPassword.getText().toString();
        confirmPassword = etConfirmPassword.getText().toString();
        email = etEmail.getText().toString();
        address = etAddress.getText().toString();
        phone = etPhone.getText().toString();
        zipCode = etZipCode.getText().toString();
        dateToStratCourse = mDateToStratCourse.getText().toString();

        if(fullName.isEmpty())
        {
            etFullName.setError("First name is required");
            return false;
        }
        if(dateToStratCourse.isEmpty())
        {
            mDateToStratCourse.setError("Select you date of birth");
            return false;
        }
        if(password.isEmpty())
        {
            etPassword.setError("Empty password");
            return false;
        }
        if(!password.equals(confirmPassword))
        {
            etConfirmPassword.setError("No matching password");
            return false;
        }
        if(!Util.isValidEmail(email))
        {
            etEmail.setError("Valid email is required");
            return false;
        }
        if(address.isEmpty())
        {
            etAddress.setError("You postal address is missing");
            return false;
        }
        if(zipCode.isEmpty())
        {
            etZipCode.setError("Zip code is required");
            return false;
        }
        if(!Util.isValidMobile(phone))
        {
            etPhone.setError("Valid phone is required");
            return false;
        }
        return true;
    }

    private void registerUser()
    {

        if (!Util.isConnectingToInternet(getApplicationContext())) {
            Util.showToastMsg(getApplicationContext(), Constants.kStringNetworkConnectivityError);
            return;
        }

        final AlertDialog dialog = Util.progressDialog(Registration.this);
        dialog.show();

        Ion.with(Registration.this).load(Constants.BASE_URL+"AddUser.php")
                .setBodyParameter("lear_name", fullName)
                .setBodyParameter("lear_email", email)
                .setBodyParameter("lear_password", password)
                .setBodyParameter("lear_phone_number", phone)
                .setBodyParameter("lear_zip_code", zipCode)
                .setBodyParameter("lear_date_to_start_course", dateToStratCourse)
                .setBodyParameter("lear_location", address)
                .setBodyParameter("latitude", learData.getLatitude()+"")
                .setBodyParameter("longitude", learData.getLongitude()+"")
                .setBodyParameter("fcm_token", token)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result)
            {
                dialog.dismiss();
                if(e == null)
                {
                    if(result != null)
                    {
                        try
                        {
                            int learID = Integer.parseInt(result);
                            learData.setLearId(String.valueOf(learID));
                            SnappyDBUtil.saveObject(Constants.USER_DATA, learData);

                            Utilities.getInstance(Registration.this).saveBooleanPreferences(Constants.PREF_IS_USER_LOGIN, true);
                            Utilities.getInstance(Registration.this).saveBooleanPreferences(Constants.PREF_IS_SET_AVAILABILITY, false);
                            Utilities.getInstance(Registration.this).saveBooleanPreferences(Constants.PREF_IS_SET_CHECKOUT, false);

                            Intent intent = new Intent(Registration.this, GearSelector.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                        catch (Exception e1)
                        {
                            Util.showToastMsg(getApplicationContext(), result);
                        }

                    }
                    return;
                }
                Util.showToastMsg(getApplicationContext(), Constants.kStringNetworkConnectivityError);
            }
        });
//
//
//        Call<ResponseBody> call = AppController.getApiService().getLearRegisterResponse(fullName,email,password,phone,zipCode, dateToStratCourse, address, learData.getLatitude(),learData.getLongitude(), token);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                dialog.dismiss();
//                String message = "";
//                try
//                {
//                    message = response.body().string();
//                    int learID = Integer.parseInt(message);
//                    learData.setLearId(String.valueOf(learID));
//                    SnappyDBUtil.saveObject(Constants.USER_DATA, learData);
//
//                    Utilities.getInstance(Registration.this).saveBooleanPreferences(Constants.PREF_IS_USER_LOGIN, true);
//                    Utilities.getInstance(Registration.this).saveBooleanPreferences(Constants.PREF_IS_SET_AVAILABILITY, false);
//                    Utilities.getInstance(Registration.this).saveBooleanPreferences(Constants.PREF_IS_SET_CHECKOUT, false);
//
//                    Intent intent = new Intent(Registration.this, GearSelector.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
//                    finish();
//                }
//                catch (Exception e)
//                {
//                    Util.showToastMsg(getApplicationContext(), message);
//                    Log.e(Constants.EXCEPTION, e.getMessage());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                dialog.show();
//                Log.e(Constants.EXCEPTION, t.getMessage());
//                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
//            }
//        });
    }

    DatePickerDialog.OnDateSetListener dateCallback = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            month = month + 1;
            String days = String.format("%02d", day);
            String months = String.format("%02d", month);
            mDateToStratCourse.setText(days + "/" + months + "/" + year);
        }
    };

}
