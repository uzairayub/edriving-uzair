package com.exceldrive.bhoomii.learner.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.exceldrive.bhoomii.Models.ModelLear.MyPackageItem;
import com.exceldrive.bhoomii.R;

import java.util.ArrayList;

public class AdapterMyPackages extends RecyclerView.Adapter<AdapterMyPackages.MyViewHolder> {

    private ArrayList<MyPackageItem> list;
    private MyPackagesCallBack callBack;

    public AdapterMyPackages(ArrayList<MyPackageItem> list, MyPackagesCallBack callBack)
    {
        this.callBack = callBack;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_packages, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position)
    {

        holder.tvTitle.setText(list.get(position).getPackageName());
        holder.tvHours.setText(list.get(position).getPackageHours() + " Hours");
        holder.tvPackageFee.setText("£ " + list.get(position).getPackageRate());
        holder.btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                callBack.onPackageSelected(position);
            }
        });
    }

    private void changeVisibility(final View view, final int visible)
    {
        view.setVisibility(visible);
    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvHours, tvPackageFee;
        Button btnPay;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvHours = itemView.findViewById(R.id.tvHours);
            tvPackageFee = itemView.findViewById(R.id.tvPackageFee);
            btnPay = itemView.findViewById(R.id.btnPay);
        }
    }

    public interface MyPackagesCallBack {
        void onPackageSelected(int position);
    }
}
