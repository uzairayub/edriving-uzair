package com.exceldrive.bhoomii.learner.Activities.Registeration;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.exceldrive.bhoomii.AppController;
import com.exceldrive.bhoomii.Models.ModelLear.LearLoginResponse;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.SnappyDBUtil;
import com.exceldrive.bhoomii.Utils.Util;
import com.exceldrive.bhoomii.Utils.Utilities;
import com.exceldrive.bhoomii.learner.Activities.LearnerMainActivity;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Availability extends AppCompatActivity implements View.OnClickListener {

    private TextView tvMondayPm, tvMondayAm, tvMondayOff, tvTuesdayPm, tvTuesdayAm, tvTuesdayOff, tvWednesdayPm, tvWednesdayAm, tvWednesdayOff,
            tvThursdayPm, tvThursdayAm, tvThursdayOff, tvFridayPm, tvFridayAm, tvFridayOff, tvSaturdayPm, tvSaturdayAm, tvSaturdayOff, tvSundayPm,
            tvSundayAm, tvSundayOff;
    private String monday, tuesday, wednesday, thursday, friday, saturday, sunday;
    private Button btnProceed;
    private LearLoginResponse learData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_availability);

        initial();
    }

    private void initial() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        learData = SnappyDBUtil.getObject(Constants.LEAR_DATA, LearLoginResponse.class);

//        Monday
        tvMondayPm = findViewById(R.id.tvMondayPm);
        tvMondayPm.setOnClickListener(this);
        tvMondayAm = findViewById(R.id.tvMondayAm);
        tvMondayAm.setOnClickListener(this);
        tvMondayOff = findViewById(R.id.tvMondayOff);
        tvMondayOff.setOnClickListener(this);
//        Tuesday
        tvTuesdayPm = findViewById(R.id.tvTuesdayPm);
        tvTuesdayPm.setOnClickListener(this);
        tvTuesdayAm = findViewById(R.id.tvTuesdayAm);
        tvTuesdayAm.setOnClickListener(this);
        tvTuesdayOff = findViewById(R.id.tvTuesdayOff);
        tvTuesdayOff.setOnClickListener(this);
//        Wednesday
        tvWednesdayPm = findViewById(R.id.tvWednesdayPm);
        tvWednesdayPm.setOnClickListener(this);
        tvWednesdayAm = findViewById(R.id.tvWednesdayAm);
        tvWednesdayAm.setOnClickListener(this);
        tvWednesdayOff = findViewById(R.id.tvWednesdayOff);
        tvWednesdayOff.setOnClickListener(this);
//        Thursday
        tvThursdayPm = findViewById(R.id.tvThursdayPm);
        tvThursdayPm.setOnClickListener(this);
        tvThursdayAm = findViewById(R.id.tvThursdayAm);
        tvThursdayAm.setOnClickListener(this);
        tvThursdayOff = findViewById(R.id.tvThursdayOff);
        tvThursdayOff.setOnClickListener(this);
//        Friday
        tvFridayPm = findViewById(R.id.tvFridayPm);
        tvFridayPm.setOnClickListener(this);
        tvFridayAm = findViewById(R.id.tvFridayAm);
        tvFridayAm.setOnClickListener(this);
        tvFridayOff = findViewById(R.id.tvFridayOff);
        tvFridayOff.setOnClickListener(this);
//        Saturday
        tvSaturdayPm = findViewById(R.id.tvSaturdayPm);
        tvSaturdayPm.setOnClickListener(this);
        tvSaturdayAm = findViewById(R.id.tvSaturdayAm);
        tvSaturdayAm.setOnClickListener(this);
        tvSaturdayOff = findViewById(R.id.tvSaturdayOff);
        tvSaturdayOff.setOnClickListener(this);
//        Sunday
        tvSundayPm = findViewById(R.id.tvSundayPm);
        tvSundayPm.setOnClickListener(this);
        tvSundayAm = findViewById(R.id.tvSundayAm);
        tvSundayAm.setOnClickListener(this);
        tvSundayOff = findViewById(R.id.tvSundayOff);
        tvSundayOff.setOnClickListener(this);

//        Buttons
        btnProceed = findViewById(R.id.btnProceed);
        btnProceed.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvMondayPm: {
                monday = "PM";
                changeIcon(tvMondayPm, tvMondayAm, tvMondayOff);
                break;
            }
            case R.id.tvMondayAm: {
                monday = "AM";
                changeIcon(tvMondayAm, tvMondayPm, tvMondayOff);
                break;
            }
            case R.id.tvMondayOff: {
                monday = "OFF";
                changeOffIcon(tvMondayOff, tvMondayPm, tvMondayAm);
                break;
            }
            case R.id.tvTuesdayPm: {
                tuesday = "PM";
                changeIcon(tvTuesdayPm, tvTuesdayAm, tvTuesdayOff);
                break;
            }
            case R.id.tvTuesdayAm: {
                tuesday = "AM";
                changeIcon(tvTuesdayAm, tvTuesdayPm, tvTuesdayOff);
                break;
            }
            case R.id.tvTuesdayOff: {
                tuesday = "OFF";
                changeOffIcon(tvTuesdayOff, tvTuesdayAm, tvTuesdayPm);
                break;
            }
            case R.id.tvWednesdayPm: {
                wednesday = "PM";
                changeIcon(tvWednesdayPm, tvWednesdayAm, tvWednesdayOff);
                break;
            }
            case R.id.tvWednesdayAm: {
                wednesday = "AM";
                changeIcon(tvWednesdayAm, tvWednesdayPm, tvWednesdayOff);
                break;
            }
            case R.id.tvWednesdayOff: {
                wednesday = "OFF";
                changeOffIcon(tvWednesdayOff, tvWednesdayAm, tvWednesdayPm);
                break;
            }
            case R.id.tvThursdayPm: {
                thursday = "PM";
                changeIcon(tvThursdayPm, tvThursdayAm, tvThursdayOff);
                break;
            }
            case R.id.tvThursdayAm: {
                thursday = "AM";
                changeIcon(tvThursdayAm, tvThursdayPm, tvThursdayOff);
                break;
            }
            case R.id.tvThursdayOff: {
                thursday = "OFF";
                changeOffIcon(tvThursdayOff, tvThursdayAm, tvThursdayPm);
                break;
            }
            case R.id.tvFridayPm: {
                friday = "PM";
                changeIcon(tvFridayPm, tvFridayAm, tvFridayOff);
                break;
            }
            case R.id.tvFridayAm: {
                friday = "AM";
                changeIcon(tvFridayAm, tvFridayPm, tvFridayOff);
                break;
            }
            case R.id.tvFridayOff: {
                friday = "OFF";
                changeOffIcon(tvFridayOff, tvFridayAm, tvFridayPm);
                break;
            }
            case R.id.tvSaturdayPm:
            {
                saturday = "PM";
                changeIcon(tvSaturdayPm, tvSaturdayAm, tvSaturdayOff);
                break;
            }
            case R.id.tvSaturdayAm:
            {
                saturday = "AM";
                changeIcon(tvSaturdayAm, tvSaturdayPm, tvSaturdayOff);
                break;
            }
            case R.id.tvSaturdayOff:
            {
                saturday = "OFF";
                changeOffIcon(tvSaturdayOff, tvSaturdayAm, tvSaturdayPm);
                break;
            }
            case R.id.tvSundayPm:
            {
                sunday = "PM";
                changeIcon(tvSundayPm, tvSundayAm, tvSundayOff);
                break;
            }
            case R.id.tvSundayAm:
            {
                sunday = "AM";
                changeIcon(tvSundayAm, tvSundayPm, tvSundayOff);
                break;
            }
            case R.id.tvSundayOff:
            {
                sunday = "OFF";
                changeOffIcon(tvSundayOff, tvSundayPm, tvSundayAm);
                break;
            }
            case R.id.btnProceed:
            {
                if(validation())
                {
                    saveAvailability();
                }
                break;
            }
        }
    }

    private void changeOffIcon(TextView setSelected, TextView textView, TextView textView1)
    {
        setSelected.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.confirm_red_icon_small, 0, 0, 0);
        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.confirm_grey_icon_small, 0, 0, 0);
        textView1.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.confirm_grey_icon_small, 0, 0, 0);
    }
    private void changeIcon(TextView setSelected, TextView textView, TextView textView1) {
        setSelected.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.confirm_green_icon_small, 0, 0, 0);
        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.confirm_grey_icon_small, 0, 0, 0);
        textView1.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.confirm_grey_icon_small, 0, 0, 0);
    }

    private void saveAvailability()
    {
        if (!Util.isConnectingToInternet(getApplicationContext())) {
            Util.showToastMsg(getApplicationContext(), Constants.kStringNetworkConnectivityError);
            return;
        }
        final AlertDialog dialog = Util.progressDialog(Availability.this);
        dialog.show();
        Call<ResponseBody> call = AppController.getApiService().getSaveAvailabilityResponse(learData.getLearId(),learData.getPackageItem().getPackageId(),monday,tuesday,wednesday,thursday,friday,saturday,sunday);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                String message = "";
                try
                {
                    message = response.body().string();
                    if(message.equalsIgnoreCase("inserted"))
                    {
                        Util.showToastMsg(getApplicationContext(), getResources().getString(R.string.saveAvailabilitySuccess));
                        Utilities.getInstance(Availability.this).saveBooleanPreferences(Constants.PREF_IS_SET_AVAILABILITY, true);
                        Utilities.getInstance(Availability.this).saveBooleanPreferences(Constants.PREF_IS_SET_CHECKOUT, false);
                        Intent intent = new Intent(Availability.this,CheckOut.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }

                }
                catch (Exception e)
                {
                    Util.showToastMsg(getApplicationContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                Log.e(Constants.EXCEPTION, t.getMessage());
                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    private boolean validation()
    {
        if(monday == null)
        {
            Util.showToastMsg(Availability.this, getResources().getString(R.string.monday_error));
            return false;
        }
        if(tuesday == null)
        {
            Util.showToastMsg(Availability.this, getResources().getString(R.string.tuesday_error));
            return false;
        }
        if(wednesday == null)
        {
            Util.showToastMsg(Availability.this, getResources().getString(R.string.wednesday_error));
            return false;
        }
        if(thursday == null)
        {
            Util.showToastMsg(Availability.this, getResources().getString(R.string.thursday_error));
            return false;
        }
        if(friday == null)
        {
            Util.showToastMsg(Availability.this, getResources().getString(R.string.friday_error));
            return false;
        }
        if(saturday == null)
        {
            Util.showToastMsg(Availability.this, getResources().getString(R.string.saturday_error));
            return false;
        }
        if(sunday == null)
        {
            Util.showToastMsg(Availability.this, getResources().getString(R.string.sunday_error));
            return false;
        }
        return true;
    }
}
