package com.exceldrive.bhoomii.learner.Activities.Registeration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.exceldrive.bhoomii.Models.ModelLear.LearLoginResponse;
import com.exceldrive.bhoomii.Models.ModelLear.PackageItem;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.SnappyDBUtil;
import com.exceldrive.bhoomii.Utils.Util;
import com.exceldrive.bhoomii.Utils.Utilities;
import com.exceldrive.bhoomii.learner.Adapters.AdapterAllPackages;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Packages extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, AdapterAllPackages.PackagesCallBack {

    private RecyclerView rvPackages;
    private ArrayList<PackageItem> list;
    private AdapterAllPackages adapter;
    private SwipeRefreshLayout refresher;
    private LearLoginResponse learData;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_packages);
        createToken();
        initial();
    }

    private void createToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(Packages.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();
            }
        });
    }

    private void initial() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        learData = SnappyDBUtil.getObject(Constants.LEAR_DATA, LearLoginResponse.class);

        refresher = findViewById(R.id.refresher);
        refresher.setOnRefreshListener(this);
        rvPackages = findViewById(R.id.rvPackages);
        rvPackages.setLayoutManager(new LinearLayoutManager(Packages.this));
        list = new ArrayList<>();
        adapter = new AdapterAllPackages(list, this);
        rvPackages.setAdapter(adapter);
        refresher.setRefreshing(true);

        if (Utilities.getInstance(Packages.this).getBooleanPreferences(Constants.PREF_IS_USER_LOGIN))
            token = learData.getFcmToken();

        loadPackages();
    }


    private void loadPackages() {
        if (!Util.isConnectingToInternet(getApplicationContext())) {
            Util.showToastMsg(getApplicationContext(), Constants.kStringNetworkConnectivityError);
            return;
        }
        String type = getIntent().getExtras().getString("gearType");
        if (type == null) {
            type = "manual";
        }
        if (type.isEmpty()) {
            type = "manual";
        }
        Ion.with(Packages.this)
                .load(Constants.BASE_URL + "LearGetAllPackages.php")
                .setBodyParameter("package_type", type)
                .setBodyParameter("fcm_token", token)
                .asJsonArray().setCallback(new FutureCallback<JsonArray>() {
            @Override
            public void onCompleted(Exception e, JsonArray result) {
                refresher.setRefreshing(false);
                if (e == null) {
                    try {
                        Type listType = new TypeToken<List<PackageItem>>() {
                        }.getType();
                        ArrayList<PackageItem> myList = new Gson().fromJson(result, listType);
                        list.addAll(myList);
                        adapter.notifyDataSetChanged();
                    } catch (Exception e1) {
                        Util.showToastMsg(getApplicationContext(), Constants.EXCEPTION_MESSAGE);
                    }

                    return;
                }
                Util.showToastMsg(getApplicationContext(), Constants.kStringNetworkConnectivityError);
            }
        });
    }

    @Override
    public void onRefresh() {
        list.clear();
        adapter = new AdapterAllPackages(list, this);
        loadPackages();
    }

    @Override
    public void onPackageSelected(int position) {
        learData.setPackageItem(list.get(position));
        SnappyDBUtil.saveObject(Constants.LEAR_DATA, learData);
//        LearLoginResponse userdata = SnappyDBUtil.getObject(Constants.LEAR_DATA, LearLoginResponse.class);
        startActivity(new Intent(Packages.this, Availability.class));
    }


}
