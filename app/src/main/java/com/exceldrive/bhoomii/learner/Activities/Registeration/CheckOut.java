package com.exceldrive.bhoomii.learner.Activities.Registeration;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.exceldrive.bhoomii.Activities.SampleActivity;
import com.exceldrive.bhoomii.Models.ModelLear.LearLoginResponse;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.SnappyDBUtil;
import com.exceldrive.bhoomii.Utils.Util;
import com.exceldrive.bhoomii.Utils.Utilities;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;

public class CheckOut extends AppCompatActivity implements View.OnClickListener {

    private Button btnPayAmount, btnPay;
    private TextView tvTitle, tvHours, tvPackageFee;
    private LearLoginResponse learData;

//    paypal
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;

    private static final String CONFIG_CLIENT_ID = "AQNAWAI9PnZH7-buQbOLeJrcIsa23xQVN3WEcqxV5M9JPL3z1SnWuNoh7w6ugIlia2qPeTkDR164HJu2";

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            .merchantName("Example Merchant")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        initial();
        paypalInit();
    }

    private void paypalInit()
    {
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
    }

    private void initial() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        learData = SnappyDBUtil.getObject(Constants.LEAR_DATA, LearLoginResponse.class);
//        TextView
        tvTitle = findViewById(R.id.tvTitle);
        tvHours = findViewById(R.id.tvHours);
        tvPackageFee = findViewById(R.id.tvPackageFee);

        tvTitle.setText(learData.getPackageItem().getPackageName());
        tvHours.setText(learData.getPackageItem().getPackageHours() + " Hours");
        tvPackageFee.setText("£ " + learData.getPackageItem().getPackageRate());

        btnPay = findViewById(R.id.btnPay);
        btnPay.setVisibility(View.GONE);
        btnPayAmount = findViewById(R.id.btnPayAmount);
        btnPayAmount.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if (v.getId() == btnPayAmount.getId())
        {
            PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
            Intent intent = new Intent(CheckOut.this, PaymentActivity.class);
            intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
            intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
            startActivityForResult(intent, 101);
        }
    }


    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal("20"), "GBP", "Package Fee",
                paymentIntent);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 101 && resultCode == RESULT_OK)
        {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null)
                {
                    try
                    {
                        Util.showToastMsg(CheckOut.this, "Payment Confirmation");
                        Utilities.getInstance(CheckOut.this).saveBooleanPreferences(Constants.PREF_IS_SET_CHECKOUT, true);
                        startActivity(new Intent(CheckOut.this, Success.class));
                        finish();
                    }
                    catch (Exception e)
                    {
                        Util.showToastMsg(CheckOut.this, e.getMessage());
                    }
                }

        }
        else if (resultCode == Activity.RESULT_CANCELED)
        {
//            Log.i(TAG, "The user canceled.");
        }
        else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID)
        {
//            Log.i(TAG,"An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }
    }
}
