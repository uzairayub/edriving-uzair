package com.exceldrive.bhoomii.learner.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.exceldrive.bhoomii.AppController;
import com.exceldrive.bhoomii.Models.ModelLear.LearLoginResponse;
import com.exceldrive.bhoomii.Models.ModelLear.LearPakcagesResponse;
import com.exceldrive.bhoomii.Models.ModelLear.MyPackageItem;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.SnappyDBUtil;
import com.exceldrive.bhoomii.Utils.Util;
import com.exceldrive.bhoomii.learner.Adapters.AdapterMyPackages;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentLearHome extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AdapterMyPackages.MyPackagesCallBack {

    private View view;
    private SwipeRefreshLayout refresher;
    private RecyclerView rvPackages;
    private ArrayList<MyPackageItem> list;
    private AdapterMyPackages adapter;
    private LearLoginResponse learData;
    private Button btnPay;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_lear_home, container, false);
        initial();
        return view;
    }

    private void initial()
    {
        learData = SnappyDBUtil.getObject(Constants.LEAR_DATA, LearLoginResponse.class);

        refresher = view.findViewById(R.id.refresher);
        refresher.setOnRefreshListener(this);
        rvPackages = view.findViewById(R.id.rvPackages);
        rvPackages.setLayoutManager(new LinearLayoutManager(getContext()));

        list = new ArrayList<>();
        adapter = new AdapterMyPackages(list, this);
        rvPackages.setAdapter(adapter);
        getLearPackages();

    }

    private void getLearPackages()
    {
        if (!Util.isConnectingToInternet(getContext()))
        {
            Util.showToastMsg(getContext(), Constants.kStringNetworkConnectivityError);
            return;
        }
        Call<ArrayList<MyPackageItem>> call = AppController.getApiService().getLeanPackagesResponse(learData.getLearId(), learData.getFcmToken());
        call.enqueue(new Callback<ArrayList<MyPackageItem>>() {
            @Override
            public void onResponse(Call<ArrayList<MyPackageItem>> call, Response<ArrayList<MyPackageItem>> response) {
                refresher.setRefreshing(false);
                try
                {
                    if(response.body().size() > 0)
                    {
                        list.addAll(response.body());
                        adapter.notifyDataSetChanged();
                    }
                }
                catch (Exception e)
                {
                    Util.showToastMsg(getContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MyPackageItem>> call, Throwable t) {
                refresher.setRefreshing(false);
                Log.e(Constants.EXCEPTION, t.getMessage());
                Util.showToastMsg(getContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    @Override
    public void onRefresh()
    {
       list.clear();
       getLearPackages();
    }

    @Override
    public void onPackageSelected(int position)
    {

    }
}
