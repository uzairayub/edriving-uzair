package com.exceldrive.bhoomii.learner.Activities.Registeration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.exceldrive.bhoomii.R;

public class Success extends AppCompatActivity implements View.OnClickListener {

    private Button btnPayAmount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        initial();
    }

    private void initial()
    {
        btnPayAmount = findViewById(R.id.btnPayAmount);
        btnPayAmount.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == btnPayAmount.getId())
        {
            Intent intent = new Intent(Success.this, GearSelector.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }
}
