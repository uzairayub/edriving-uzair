package com.exceldrive.bhoomii.learner.Activities.Registeration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.exceldrive.bhoomii.Models.ModelLear.LearLoginResponse;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.SnappyDBUtil;

public class GearSelector extends AppCompatActivity implements View.OnClickListener {

    private TextView tvAuto, tvManual;
    private Button btnContinue;
    private EditText etZipCode;
    private String gearType = "auto";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gear_selector);

        initial();
    }


    private void initial()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
//        TextView
        tvAuto = findViewById(R.id.tvAuto);
        tvAuto.setOnClickListener(this);
        tvManual = findViewById(R.id.tvManual);
        tvManual.setOnClickListener(this);

//        EditText
        etZipCode = findViewById(R.id.etZipCode);
//        Button
        btnContinue = findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvAuto:
            {
                gearType = "auto";
                tvAuto.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.confirm_green_icon_small,0);
                tvManual.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.confirm_grey_icon_small,0);
                break;
            }
            case R.id.tvManual:
            {
                gearType = "manual";
                tvAuto.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.confirm_grey_icon_small,0);
                tvManual.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.confirm_green_icon_small,0);
                break;
            }
            case R.id.btnContinue:
            {
                Intent intent = new Intent(GearSelector.this, Packages.class);
                intent.putExtra("gearType",gearType);
                startActivity(intent);
                break;
            }
        }
    }

}
