package com.exceldrive.bhoomii.learner.Activities.Registeration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.exceldrive.bhoomii.Models.ModelLear.LearLoginResponse;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.GPSTracker;
import com.exceldrive.bhoomii.Utils.SnappyDBUtil;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, PlaceSelectionListener, View.OnClickListener, GoogleMap.OnCameraIdleListener {

    private ImageView centerView;
    private TextView tvAddress;
    private LinearLayout llCenter;
    private ImageButton ibMyLocation;
    private Button btnConfirmPickup;
    private GoogleMap mMap;
    private MapView mapView;
    private LatLng latLng;
    private GPSTracker gpsTracker;
    private LearLoginResponse learData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        initial(savedInstanceState);
    }

    private void initial(Bundle savedInstanceState)
    {
        learData = new LearLoginResponse();
        gpsTracker = new GPSTracker(MapsActivity.this);
        centerView = findViewById(R.id.centerView);
        tvAddress = findViewById(R.id.tvAddress);
        llCenter = findViewById(R.id.llCenter);
        ibMyLocation = findViewById(R.id.ibMyLocation);
        btnConfirmPickup = findViewById(R.id.btnConfirmPickup);
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        ibMyLocation.setOnClickListener(this);
        btnConfirmPickup.setOnClickListener(this);
        ibMyLocation.callOnClick();
    }
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onPlaceSelected(Place place)
    {
        if( mMap!= null)
        {
            latLng = place.getLatLng();
            animateCamera(latLng);
            mMap.setOnCameraIdleListener(this);
            String address = gpsTracker.getGeoCoderAddress(MapsActivity.this, latLng);
            tvAddress.setText(address);
            llCenter.setVisibility(View.VISIBLE);
        }
    }

    private void animateCamera(LatLng latLng)
    {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,17));
    }
    @Override
    public void onError(Status status)
    {

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ibMyLocation:
            {
                if(gpsTracker.canGetLocation())
                {
                    latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                    animateCamera(latLng);
                    mMap.setOnCameraIdleListener(this);
                    String address = gpsTracker.getGeoCoderAddress(MapsActivity.this, latLng);
                    tvAddress.setText(address);
                    llCenter.setVisibility(View.VISIBLE);
                }
                else
                {
                    gpsTracker.showSettingsAlert();
                }
                break;
            }
            case R.id.btnConfirmPickup:
            {
                if(latLng != null)
                {
                    latLng = mMap.getCameraPosition().target;
                    learData.setLatitude(latLng.latitude);
                    learData.setLongitude(latLng.longitude);
                    SnappyDBUtil.saveObject(Constants.LEAR_DATA, learData);
                    startActivity(new Intent(MapsActivity.this, Registration.class));
                }
                break;
            }
        }
    }

    @Override
    public void onCameraIdle()
    {
        latLng = mMap.getCameraPosition().target;
        String address = gpsTracker.getGeoCoderAddress(MapsActivity.this, latLng);
        tvAddress.setText(address);
        llCenter.setVisibility(View.VISIBLE);
    }
}
