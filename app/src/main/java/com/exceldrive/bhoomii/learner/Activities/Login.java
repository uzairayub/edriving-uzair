package com.exceldrive.bhoomii.learner.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.exceldrive.bhoomii.learner.Activities.Registeration.MapsActivity;
import com.exceldrive.bhoomii.learner.Activities.Registeration.Registration;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.exceldrive.bhoomii.AppController;
import com.exceldrive.bhoomii.Models.ModelLear.LearLoginResponse;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.SnappyDBUtil;
import com.exceldrive.bhoomii.Utils.Util;
import com.exceldrive.bhoomii.Utils.Utilities;
import com.exceldrive.bhoomii.learner.Activities.Registeration.GearSelector;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Login extends AppCompatActivity implements View.OnClickListener {

    private EditText etEmail, etPassword;
    private String email, password;
    private TextView tvSignUp;
    private Button btnLogin;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        createToken();
        initial();
    }

    private void createToken()
    {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( Login.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();
            }
        });
    }

    private void initial()
    {

        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        tvSignUp = findViewById(R.id.tvSignUp);
        tvSignUp.setOnClickListener(this);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.tvSignUp:
            {
                if(checkPermission())
                {
                    showDialog();
                }
                break;
            }
            case R.id.btnLogin:
            {
                if(validation())
                {
                    loginUser();
                }
                break;
            }
        }
    }

    private void loginUser()
    {
        if (!Util.isConnectingToInternet(getApplicationContext())) {
            Util.showToastMsg(getApplicationContext(), Constants.kStringNetworkConnectivityError);
            return;
        }

        final AlertDialog dialog = Util.progressDialog(Login.this);
        dialog.show();
        Call<ArrayList<LearLoginResponse>> call = AppController.getApiService().getLearLoginResponse(email,password,token);
        call.enqueue(new Callback<ArrayList<LearLoginResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<LearLoginResponse>> call, Response<ArrayList<LearLoginResponse>> response) {
                dialog.dismiss();
                try {
                    if (response.body().size() > 0)
                    {

                        SnappyDBUtil.saveObject(Constants.LEAR_DATA, response.body().get(0));
//                        Util.showToastMsg(getApplicationContext(), response.body().getMessage());
                        Utilities.getInstance(Login.this).saveBooleanPreferences(Constants.PREF_IS_USER_LOGIN, true);
                        Utilities.getInstance(Login.this).saveBooleanPreferences(Constants.PREF_IS_SET_AVAILABILITY, true);
                        Utilities.getInstance(Login.this).saveBooleanPreferences(Constants.PREF_IS_SET_CHECKOUT, true);
                        startActivity(new Intent(Login.this, LearnerMainActivity.class));
                        finish();
                    }
                    else
                    {
//                        Util.showToastMsg(getApplicationContext(), getResources().getString(R.string.));
                    }
                } catch (Exception e) {
                    Util.showToastMsg(getApplicationContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<LearLoginResponse>> call, Throwable t) {
                dialog.dismiss();
                Log.e(Constants.EXCEPTION, t.getMessage());
                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    private boolean validation()
    {
        email = etEmail.getText().toString().trim();
        password = etPassword.getText().toString();
        if(token == null)
        {
            createToken();
        }
        if(!Util.isValidEmail(email))
        {
            etEmail.setError("Valid email is required");
            return false;
        }
        if(password.isEmpty())
        {
            etPassword.setError("Valid password is required");
            return false;
        }
        return true;
    }


    private boolean checkPermission()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if(checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                return true;
            }
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            return false;
        }
        return true;
    }

    private void showDialog()
    {
        final AlertDialog.Builder alert = new AlertDialog.Builder(Login.this);
        alert.setTitle("Location Alert");
        alert.setMessage("Before you register please select your location");
        alert.setIcon(android.R.drawable.ic_dialog_alert);
        alert.setPositiveButton("Select Location", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                startActivity(new Intent(Login.this, MapsActivity.class));
            } });


        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                 finish();
            } });
        alert.show();
        // Toast.makeText(inflater.getContext(), "Hello", Toast.LENGTH_SHORT).show();
    }
}
