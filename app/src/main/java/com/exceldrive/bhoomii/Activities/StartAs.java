package com.exceldrive.bhoomii.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.Utilities;
import com.exceldrive.bhoomii.instructor.activities.InsHomePage;
import com.exceldrive.bhoomii.instructor.activities.InsSignUp;
import com.exceldrive.bhoomii.learner.Activities.LearnerMainActivity;
import com.exceldrive.bhoomii.learner.Activities.Login;
import com.exceldrive.bhoomii.learner.Activities.Registeration.CheckOut;
import com.exceldrive.bhoomii.learner.Activities.Registeration.GearSelector;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class StartAs extends AppCompatActivity implements View.OnClickListener {

    private TextView tvDriver, tvLearner;
    private Button btnContinue;
    private String selectedUserType = "driver";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_as);

        createToken();
        if(Utilities.getInstance(StartAs.this).getBooleanPreferences(Constants.PREF_IS_INS_LOGIN))
        {
            Log.d("mylog", "instructor login");
            startActivity(new Intent(StartAs.this, InsHomePage.class));
            finish();
            return;
        }

        if(Utilities.getInstance(StartAs.this).getBooleanPreferences(Constants.PREF_IS_USER_LOGIN)) {
            Log.d("mylog", "user login");
            if(Utilities.getInstance(StartAs.this).getBooleanPreferences(Constants.PREF_IS_SET_AVAILABILITY)) {
                if(Utilities.getInstance(StartAs.this).getBooleanPreferences(Constants.PREF_IS_SET_CHECKOUT))
                {
                    startActivity(new Intent(StartAs.this, LearnerMainActivity.class));
                }
                else
                {
                    startActivity(new Intent(StartAs.this, CheckOut.class));
                }
            } else {
                startActivity(new Intent(StartAs.this, GearSelector.class));
            }
            finish();
            return;
        }

        String type = Utilities.getInstance(StartAs.this).getStringPreferences(Constants.SELECTED_USER_Type);
        if(type.equalsIgnoreCase("driver"))
        {
            startActivity(new Intent(StartAs.this, InsSignUp.class));
            finish();
            return;
        }
        else if(type.equalsIgnoreCase("learner"))
        {
            startActivity(new Intent(StartAs.this, Login.class));
            finish();
            return;
        }

        initial();
    }


    private void createToken()
    {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( StartAs.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                Log.d("token", token);
            }
        });
    }

    private void initial() {

        tvDriver = findViewById(R.id.tvDriver);
        tvDriver.setOnClickListener(this);
        tvLearner = findViewById(R.id.tvLearner);
        tvLearner.setOnClickListener(this);

        btnContinue = findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.tvDriver:
            {
                selectedUserType = "driver";
                tvDriver.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.confirm_green_icon_small,0);
                tvLearner.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.confirm_grey_icon_small,0);
                break;
            }
            case R.id.tvLearner:
            {
                selectedUserType = "learner";
                tvDriver.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.confirm_grey_icon_small,0);
                tvLearner.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.confirm_green_icon_small,0);
                break;
            }
            case R.id.btnContinue:
            {
                if(selectedUserType.equalsIgnoreCase("driver"))
                {
                    Utilities.getInstance(StartAs.this).saveStringPreferences(Constants.SELECTED_USER_Type,selectedUserType);
                    startActivity(new Intent(StartAs.this, InsSignUp.class));
                    finish();
                    return;
                }
                else if(selectedUserType.equalsIgnoreCase("learner"))
                {
                    Utilities.getInstance(StartAs.this).saveStringPreferences(Constants.SELECTED_USER_Type,selectedUserType);
                    startActivity(new Intent(StartAs.this, Login.class));
                    finish();
                    return;
                }
                finish();
                break;
            }
        }
    }
}
