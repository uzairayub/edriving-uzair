package com.exceldrive.bhoomii.Utils;

public interface Constants {

    //bhoomii start
    String LOAD_AVAILABLE_REQUESTS = "https://www.gezonderelatiemeteten.nl/androidapp/edriving/GetAllRequests.php";
    String REGISTER_INSTRUCTOR = "https://www.gezonderelatiemeteten.nl/androidapp/edriving/AddInstructor.php";
    String UPDATE_REQUEST = "https://www.gezonderelatiemeteten.nl/androidapp/edriving/InsUpdateRequest.php";
    String LOAD_PROFILE  = "https://www.gezonderelatiemeteten.nl/androidapp/edriving/InsGetProfile.php";
    String UPDATE_PASSWORD  = "https://www.gezonderelatiemeteten.nl/androidapp/edriving/CheckPassword.php";
    String LOGIN  = "https://www.gezonderelatiemeteten.nl/androidapp/edriving/InstructorLogin.php";
    //bhoomii end

    String BASE_URL = "https://www.gezonderelatiemeteten.nl/androidapp/edriving/";
    String kStringNetworkConnectivityError = "Please make sure your device is connected with internet.";
    String EXCEPTION = "Exception";
    String EXCEPTION_MESSAGE = "Something went wrong";
    String SERVER_EXCEPTION_MESSAGE = "Something went wrong, server not responding";

    String mFIELD = "fields";
    String mPARAMETERS = "id,name,email,gender,birthday,picture.type(large),cover,age_range";

    // Snappy save User data
    String USER_DATA = "userData";
    String LOGIN_DATA = "loginData";

    String LEAR_DATA = "leanData";

    // Snappy save my stories data
    String STORY_DATA = "myStoriesData";

    // Preferences
    String PREF_IS_USER_LOGIN = "isLearLogIn";
    String PREF_IS_INS_LOGIN = "isInsLogIn";
    String PREF_IS_SET_AVAILABILITY= "isSetAvailability";
    String PREF_IS_SET_CHECKOUT= "isSetCheckOut";
    String PREF_FCM_TOKEN = "token";
    String SELECTED_USER_Type = "selectedUserType";

    //Constants Extras
    String USER_NMAE = "name";
    String USER_EMAIL = "email";
    String USER_GENDER = "gender";
    String USER_ID = "id";
    String USER_AUTH_METHOD = "authMethod";
    String USER_ACCESS_TOKEN = "accessToken";


//    intents parser
    String STEP = "step";
}
