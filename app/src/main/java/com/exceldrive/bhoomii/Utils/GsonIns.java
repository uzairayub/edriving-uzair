package com.exceldrive.bhoomii.Utils;

import com.google.gson.annotations.SerializedName;

public class GsonIns {

    @SerializedName("lear_id")
    private String learId;
    @SerializedName("lear_name")
    private String learName;
    @SerializedName("lear_phone_number")
    private String learPhoneNumber;
    @SerializedName("lear_email")
    private String learEmail;
    @SerializedName("lear_zip_code")
    private String learPostalCode;
    //    @SerializedName("email")
//    private String learAddress;
    @SerializedName("lear_location")
    private String learLocation;


    @SerializedName("package_id")
    private String packageId;
    @SerializedName("package_name")
    private String packageName;
    @SerializedName("package_hours")
    private String packageHours;
    @SerializedName("package_rate")
    private String packageRate;
    @SerializedName("package_description")
    private String packageDesc;
    @SerializedName("package_type")
    private String lisenceType;

    @SerializedName("lear_dob")
    private String dateToStartCourse;



    @SerializedName("req_id")
    private String reqId;
    @SerializedName("req_status")
    private String reqStatus;
    @SerializedName("req_acceptor_id")
    private String reqAcceptorId;
    @SerializedName("req_accepted_date")
    private String reqAcceptedDate;
    @SerializedName("monday")
    private String availMonday;
    @SerializedName("tuesday")
    private String availTuesday;
    @SerializedName("wednesday")
    private String availWednesday;
    @SerializedName("thursday")
    private String availThursday;
    @SerializedName("friday")
    private String availFriday;
    @SerializedName("saturday")
    private String availSaturday;
    @SerializedName("sunday")
    private String availSunday;

    public GsonIns(String learId, String learName, String learPhoneNumber, String learEmail, String learPostalCode, String learLocation, String packageId, String packageName, String packageHours, String packageRate, String packageDesc, String lisenceType, String dateToStartCourse, String reqId, String reqStatus, String reqAcceptorId, String availMonday, String availTuesday, String availWednesday, String availThursday, String availFriday, String availSaturday, String availSunday) {
        this.learId = learId;
        this.learName = learName;
        this.learPhoneNumber = learPhoneNumber;
        this.learEmail = learEmail;
        this.learPostalCode = learPostalCode;
        this.learLocation = learLocation;
        this.packageId = packageId;
        this.packageName = packageName;
        this.packageHours = packageHours;
        this.packageRate = packageRate;
        this.packageDesc = packageDesc;
        this.lisenceType = lisenceType;
        this.dateToStartCourse=dateToStartCourse;
        this.reqId = reqId;
        this.reqStatus = reqStatus;
        this.reqAcceptorId = reqAcceptorId;
        this.availMonday = availMonday;
        this.availTuesday = availTuesday;
        this.availWednesday = availWednesday;
        this.availThursday = availThursday;
        this.availFriday = availFriday;
        this.availSaturday = availSaturday;
        this.availSunday = availSunday;
    }

    public String getLearId() {
        return learId;
    }

    public String getLearName() {
        return learName;
    }

    public String getLearPhoneNumber() {
        return learPhoneNumber;
    }

    public String getLearEmail() {
        return learEmail;
    }

    public String getLearPostalCode() {
        return learPostalCode;
    }

    public String getLearLocation() {
        return learLocation;
    }

    public String getPackageId() {
        return packageId;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getPackageHours() {
        return packageHours;
    }

    public String getPackageRate() {
        return packageRate;
    }

    public String getPackageDesc() {
        return packageDesc;
    }

    public String getLisenceType() {
        return lisenceType;
    }

    public String getDateToStartCourse() {
        return dateToStartCourse;
    }

    public String getReqId() {
        return reqId;
    }

    public String getReqStatus() {
        return reqStatus;
    }

    public String getReqAcceptorId() {
        return reqAcceptorId;
    }

    public String getAvailMonday() {
        return availMonday;
    }

    public String getAvailTuesday() {
        return availTuesday;
    }

    public String getAvailWednesday() {
        return availWednesday;
    }

    public String getAvailThursday() {
        return availThursday;
    }

    public String getAvailFriday() {
        return availFriday;
    }

    public String getAvailSaturday() {
        return availSaturday;
    }

    public String getAvailSunday() {
        return availSunday;
    }

    public String getReqAcceptedDate() {
        return reqAcceptedDate;
    }
}