package com.exceldrive.bhoomii.Models.ModelLear;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PackageItem implements Serializable {

	@SerializedName("package_rate")
	private String packageRate;

	@SerializedName("package_name")
	private String packageName;

	@SerializedName("package_hours")
	private String packageHours;

	@SerializedName("package_id")
	private String packageId;

	@SerializedName("package_type")
	private String packageType;

	@SerializedName("visibility")
	private boolean visibility;

	@SerializedName("package_description")
	private String packageDescription;

	public void setPackageRate(String packageRate){
		this.packageRate = packageRate;
	}

	public String getPackageRate(){
		return packageRate;
	}

	public void setPackageName(String packageName){
		this.packageName = packageName;
	}

	public String getPackageName(){
		return packageName;
	}

	public void setPackageHours(String packageHours){
		this.packageHours = packageHours;
	}

	public String getPackageHours(){
		return packageHours;
	}

	public void setPackageId(String packageId){
		this.packageId = packageId;
	}

	public String getPackageId(){
		return packageId;
	}

	public void setPackageType(String packageType){
		this.packageType = packageType;
	}

	public String getPackageType(){
		return packageType;
	}

	public void setPackageDescription(String packageDescription){
		this.packageDescription = packageDescription;
	}

	public String getPackageDescription(){
		return packageDescription;
	}

	public boolean isVisibility() {
		return visibility;
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}

	@Override
 	public String toString(){
		return 
			"PackageItem{" +
			"package_rate = '" + packageRate + '\'' + 
			",package_name = '" + packageName + '\'' + 
			",package_hours = '" + packageHours + '\'' + 
			",package_id = '" + packageId + '\'' + 
			",package_type = '" + packageType + '\'' + 
			",package_description = '" + packageDescription + '\'' + 
			"}";
		}
}