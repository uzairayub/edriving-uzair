package com.exceldrive.bhoomii.Models.ModelLear;

import com.google.gson.annotations.SerializedName;

public class LearLoginResponse {

	@SerializedName("lear_phone_number")
	private String learPhoneNumber;

	@SerializedName("lear_zip_code")
	private String learZipCode;

	@SerializedName("lear_email")
	private String learEmail;

	@SerializedName("lear_id")
	private String learId;

	@SerializedName("lear_location")
	private String learLocation;

	@SerializedName("lear_name")
	private String learName;

	@SerializedName("lear_password")
	private String learPassword;

	@SerializedName("fcm_token")
	private String fcmToken;

	@SerializedName("lear_dob")
	private String learDob;

	@SerializedName("gear")
	private String gear;

	@SerializedName("selectedPackage")
	private PackageItem PackageItem;

	@SerializedName("address")
	private String address;

	@SerializedName("lat")
	private double latitude;

	@SerializedName("lng")
	private double longitude;


	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getGear() {
		return gear;
	}

	public void setGear(String gear) {
		this.gear = gear;
	}

	public com.exceldrive.bhoomii.Models.ModelLear.PackageItem getPackageItem() {
		return PackageItem;
	}

	public void setPackageItem(com.exceldrive.bhoomii.Models.ModelLear.PackageItem packageItem) {
		PackageItem = packageItem;
	}

	public void setLearPhoneNumber(String learPhoneNumber){
		this.learPhoneNumber = learPhoneNumber;
	}

	public String getLearPhoneNumber(){
		return learPhoneNumber;
	}

	public void setLearZipCode(String learZipCode){
		this.learZipCode = learZipCode;
	}

	public String getLearZipCode(){
		return learZipCode;
	}

	public void setLearEmail(String learEmail){
		this.learEmail = learEmail;
	}

	public String getLearEmail(){
		return learEmail;
	}

	public void setLearId(String learId){
		this.learId = learId;
	}

	public String getLearId(){
		return learId;
	}

	public void setLearLocation(String learLocation){
		this.learLocation = learLocation;
	}

	public String getLearLocation(){
		return learLocation;
	}

	public void setLearName(String learName){
		this.learName = learName;
	}

	public String getLearName(){
		return learName;
	}

	public void setLearPassword(String learPassword){
		this.learPassword = learPassword;
	}

	public String getLearPassword(){
		return learPassword;
	}

	public void setFcmToken(String fcmToken){
		this.fcmToken = fcmToken;
	}

	public String getFcmToken(){
		return fcmToken;
	}

	public void setLearDob(String learDob){
		this.learDob = learDob;
	}

	public String getLearDob(){
		return learDob;
	}

	@Override
 	public String toString(){
		return 
			"LearLoginResponse{" +
			"lear_phone_number = '" + learPhoneNumber + '\'' + 
			",lear_zip_code = '" + learZipCode + '\'' + 
			",lear_email = '" + learEmail + '\'' + 
			",lear_id = '" + learId + '\'' + 
			",lear_location = '" + learLocation + '\'' + 
			",lear_name = '" + learName + '\'' + 
			",lear_password = '" + learPassword + '\'' + 
			",fcm_token = '" + fcmToken + '\'' + 
			",lear_dob = '" + learDob + '\'' + 
			"}";
		}
}