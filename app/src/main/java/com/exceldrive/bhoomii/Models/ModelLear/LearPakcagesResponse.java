package com.exceldrive.bhoomii.Models.ModelLear;

import com.google.gson.annotations.SerializedName;

public class LearPakcagesResponse {

	@SerializedName("saturday")
	private String saturday;

	@SerializedName("lear_location")
	private String learLocation;

	@SerializedName("package_id")
	private String packageId;

	@SerializedName("package_type")
	private String packageType;

	@SerializedName("req_id")
	private String reqId;

	@SerializedName("lear_phone_number")
	private String learPhoneNumber;

	@SerializedName("lear_id")
	private String learId;

	@SerializedName("fcm_token")
	private String fcmToken;

	@SerializedName("wednesday")
	private String wednesday;

	@SerializedName("friday")
	private String friday;

	@SerializedName("req_acceptor_id")
	private String reqAcceptorId;

	@SerializedName("monday")
	private String monday;

	@SerializedName("lear_zip_code")
	private String learZipCode;

	@SerializedName("req_status")
	private String reqStatus;

	@SerializedName("package_rate")
	private String packageRate;

	@SerializedName("lear_name")
	private String learName;

	@SerializedName("thursday")
	private String thursday;

	@SerializedName("lear_dob")
	private String learDob;

	@SerializedName("package_description")
	private String packageDescription;

	@SerializedName("sunday")
	private String sunday;

	@SerializedName("tuesday")
	private String tuesday;

	@SerializedName("lear_email")
	private String learEmail;

	@SerializedName("lear_password")
	private String learPassword;

	@SerializedName("package_name")
	private String packageName;

	@SerializedName("package_hours")
	private String packageHours;

	public void setSaturday(String saturday){
		this.saturday = saturday;
	}

	public String getSaturday(){
		return saturday;
	}

	public void setLearLocation(String learLocation){
		this.learLocation = learLocation;
	}

	public String getLearLocation(){
		return learLocation;
	}

	public void setPackageId(String packageId){
		this.packageId = packageId;
	}

	public String getPackageId(){
		return packageId;
	}

	public void setPackageType(String packageType){
		this.packageType = packageType;
	}

	public String getPackageType(){
		return packageType;
	}

	public void setReqId(String reqId){
		this.reqId = reqId;
	}

	public String getReqId(){
		return reqId;
	}

	public void setLearPhoneNumber(String learPhoneNumber){
		this.learPhoneNumber = learPhoneNumber;
	}

	public String getLearPhoneNumber(){
		return learPhoneNumber;
	}

	public void setLearId(String learId){
		this.learId = learId;
	}

	public String getLearId(){
		return learId;
	}

	public void setFcmToken(String fcmToken){
		this.fcmToken = fcmToken;
	}

	public String getFcmToken(){
		return fcmToken;
	}

	public void setWednesday(String wednesday){
		this.wednesday = wednesday;
	}

	public String getWednesday(){
		return wednesday;
	}

	public void setFriday(String friday){
		this.friday = friday;
	}

	public String getFriday(){
		return friday;
	}

	public void setReqAcceptorId(String reqAcceptorId){
		this.reqAcceptorId = reqAcceptorId;
	}

	public String getReqAcceptorId(){
		return reqAcceptorId;
	}

	public void setMonday(String monday){
		this.monday = monday;
	}

	public String getMonday(){
		return monday;
	}

	public void setLearZipCode(String learZipCode){
		this.learZipCode = learZipCode;
	}

	public String getLearZipCode(){
		return learZipCode;
	}

	public void setReqStatus(String reqStatus){
		this.reqStatus = reqStatus;
	}

	public String getReqStatus(){
		return reqStatus;
	}

	public void setPackageRate(String packageRate){
		this.packageRate = packageRate;
	}

	public String getPackageRate(){
		return packageRate;
	}

	public void setLearName(String learName){
		this.learName = learName;
	}

	public String getLearName(){
		return learName;
	}

	public void setThursday(String thursday){
		this.thursday = thursday;
	}

	public String getThursday(){
		return thursday;
	}

	public void setLearDob(String learDob){
		this.learDob = learDob;
	}

	public String getLearDob(){
		return learDob;
	}

	public void setPackageDescription(String packageDescription){
		this.packageDescription = packageDescription;
	}

	public String getPackageDescription(){
		return packageDescription;
	}

	public void setSunday(String sunday){
		this.sunday = sunday;
	}

	public String getSunday(){
		return sunday;
	}

	public void setTuesday(String tuesday){
		this.tuesday = tuesday;
	}

	public String getTuesday(){
		return tuesday;
	}

	public void setLearEmail(String learEmail){
		this.learEmail = learEmail;
	}

	public String getLearEmail(){
		return learEmail;
	}

	public void setLearPassword(String learPassword){
		this.learPassword = learPassword;
	}

	public String getLearPassword(){
		return learPassword;
	}

	public void setPackageName(String packageName){
		this.packageName = packageName;
	}

	public String getPackageName(){
		return packageName;
	}

	public void setPackageHours(String packageHours){
		this.packageHours = packageHours;
	}

	public String getPackageHours(){
		return packageHours;
	}

	@Override
 	public String toString(){
		return 
			"LearPakcagesResponse{" +
			"saturday = '" + saturday + '\'' + 
			",lear_location = '" + learLocation + '\'' + 
			",package_id = '" + packageId + '\'' + 
			",package_type = '" + packageType + '\'' + 
			",req_id = '" + reqId + '\'' + 
			",lear_phone_number = '" + learPhoneNumber + '\'' + 
			",lear_id = '" + learId + '\'' + 
			",fcm_token = '" + fcmToken + '\'' + 
			",wednesday = '" + wednesday + '\'' + 
			",friday = '" + friday + '\'' + 
			",req_acceptor_id = '" + reqAcceptorId + '\'' + 
			",monday = '" + monday + '\'' + 
			",lear_zip_code = '" + learZipCode + '\'' + 
			",req_status = '" + reqStatus + '\'' + 
			",package_rate = '" + packageRate + '\'' + 
			",lear_name = '" + learName + '\'' + 
			",thursday = '" + thursday + '\'' + 
			",lear_dob = '" + learDob + '\'' + 
			",package_description = '" + packageDescription + '\'' + 
			",sunday = '" + sunday + '\'' + 
			",tuesday = '" + tuesday + '\'' + 
			",lear_email = '" + learEmail + '\'' + 
			",lear_password = '" + learPassword + '\'' + 
			",package_name = '" + packageName + '\'' + 
			",package_hours = '" + packageHours + '\'' + 
			"}";
		}
}