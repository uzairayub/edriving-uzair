package com.exceldrive.bhoomii.NetWorkClient;

import com.exceldrive.bhoomii.Models.ModelLear.LearLoginResponse;
import com.exceldrive.bhoomii.Models.ModelLear.LearPakcagesResponse;
import com.exceldrive.bhoomii.Models.ModelLear.MyPackageItem;
import com.exceldrive.bhoomii.Models.ModelLear.PackageItem;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("account-verify")
    Call<LearLoginResponse> getLearnerLoginResponse(@Field("lear_email") String learEmail,
                                                    @Field("lear_password") String learPassword,
                                                    @Field("fcm_token") String learToken);


    @FormUrlEncoded
    @POST("LearGetAllPackages.php")
    Call<ArrayList<PackageItem>> getPackagesResponse(@Field("package_type") String packageType,
                                                     @Field("fcm_token") String fcmToken);

    @FormUrlEncoded
    @POST("LearLogin.php")
    Call<ArrayList<LearLoginResponse>> getLearLoginResponse(@Field("lear_email") String learEmail,
                                                            @Field("lear_password") String learPassword,
                                                            @Field("fcm_token") String fcmToken);
    @FormUrlEncoded
    @POST("LearGetMyPackages.php")
    Call<ArrayList<MyPackageItem>> getLeanPackagesResponse(@Field("lear_id") String learId,
                                                           @Field("fcm_token") String fcmToken);
    @FormUrlEncoded
    @POST("AddUser.php")
    Call<ResponseBody> getLearRegisterResponse(@Field("lear_name") String learName,
                                               @Field("lear_email") String learEmail,
                                               @Field("lear_password") String learPassword,
                                               @Field("lear_phone_number") String learPhone,
                                               @Field("lear_zip_code") String learZipCode,
                                               @Field("lear_date_to_start_course") String learDob,
                                               @Field("lear_location") String learLocation,
                                               @Field("latitude") double latitude,
                                               @Field("longitude") double longitude,
                                               @Field("fcm_token") String fcmToken);

    @FormUrlEncoded
    @POST("Request.php")
    Call<ResponseBody> getSaveAvailabilityResponse(@Field("learner_id") String learner_id,
                                                   @Field("package_id") String packageId,
                                                   @Field("monday") String monday,
                                                   @Field("tuesday") String tuesday,
                                                   @Field("wednesday") String wednesday,
                                                   @Field("thursday") String thursday,
                                                   @Field("friday") String friday,
                                                   @Field("saturday") String saturday,
                                                   @Field("sunday") String sunday);



}
