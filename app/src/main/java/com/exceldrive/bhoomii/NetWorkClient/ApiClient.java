package com.exceldrive.bhoomii.NetWorkClient;


import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.GsonUtil;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    // base url for the API Client
    private static final String BASE_URL = Constants.BASE_URL;
    // Retrofit client instance
    private static Retrofit retrofit = null;

    // Singleton initialization of Retorfit client instance
    public static Retrofit getClient() {
        if (retrofit == null) {
            // OkHTTPClient Client initialization

            OkHttpClient httpClient;
            try {
                // Create a trust manager that does not validate certificate chains
                final TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            @Override
                            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            }

                            @Override
                            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            }

                            @Override
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return new java.security.cert.X509Certificate[]{};
                            }
                        }
                };

                // Install the all-trusting trust manager
                final SSLContext sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
                // Create an ssl socket factory with our all-trusting manager
                final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

                // OkHTTPClient Client initialization
                httpClient = new OkHttpClient.Builder()
                        // Connection timeout 40 seconds
                        .connectTimeout(40, TimeUnit.SECONDS)
                        // Socket Read timeout 40 seconds
                        .readTimeout(40, TimeUnit.SECONDS)
                        .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
                        .hostnameVerifier(new HostnameVerifier() {
                            @Override
                            public boolean verify(String hostname, SSLSession session) {
                                return true;
                            }
                        })
                        // Add HTTP logging interceptor at body level
                        .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                        .build();

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            retrofit = new Retrofit.Builder()
                    // Add GSON parser
                    .addConverterFactory(GsonConverterFactory.create(GsonUtil.ins().getGson()))
                    // Set baseurl
                    .baseUrl(BASE_URL)
                    // set httpClient OkHTTPClient.
                    .client(httpClient)
                    .build();
        }
        return retrofit;
    }
}