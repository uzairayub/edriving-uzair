package com.exceldrive.bhoomii.instructor.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.exceldrive.bhoomii.AppController;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.GsonIns;
import com.exceldrive.bhoomii.Utils.RecyclerItemClickListener;
import com.exceldrive.bhoomii.Utils.Utilities;
import com.exceldrive.bhoomii.instructor.adapters.InsAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class InsHomePage extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView insPackages;
    private List<GsonIns> parentList, childList;
    private InsAdapter mInsAdapter;
    private LinearLayout container;
    private String claimedJobs = "no", insId, firstName, lastName;
    private TextView requestsCounter;
    private RelativeLayout refresh;
    private TextView insFullName;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ins_home_page);

        if (getSharedPreferences("applevel", MODE_PRIVATE).getString("is_registered", "no").equals("no")) {
            startActivity(new Intent(InsHomePage.this, InsSignUp.class));
            finish();
        } else {
            init();
        }
    }

    private void init() {
        //tool bar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        container = (LinearLayout) findViewById(R.id.container);
        requestsCounter = (TextView) findViewById(R.id.request_counter);
        refresh = (RelativeLayout) findViewById(R.id.refresh);


        insId = getSharedPreferences("applevel", MODE_PRIVATE).getString("ins_id", "null");
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                claimedJobs = "no";
                getMyAllPackages();
            }
        });

        //navigation drawer
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.ins_drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        //change the navigation drawer icon
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.filter_icon, getTheme());
        toggle.setHomeAsUpIndicator(drawable);

        //As we have changed the nav drawer icon so we will have to set the click listner
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        parentList = new LinkedList<>();
        displayRightNavigation();

        //set the instructor name inside the navigarion drawer on Nav-header
        firstName = getSharedPreferences("applevel", MODE_PRIVATE).getString("ins_first_name", "First Name");
        lastName = getSharedPreferences("applevel", MODE_PRIVATE).getString("ins_last_name", "Last Name");
        View header = navigationView.getHeaderView(0);
        insFullName = (TextView) header.findViewById(R.id.full_name);
        insFullName.setText(firstName + " " + lastName);

        if (AppController.haveNetworkConnection(InsHomePage.this)) {
            getMyAllPackages();
        } else {
            openDialogs();
        }
    }

    private void getMyAllPackages() {
        final ProgressDialog loadingdialog;
        loadingdialog = new ProgressDialog(InsHomePage.this);
        loadingdialog.setTitle("Loading Packages");
        loadingdialog.setMessage("Please wait..");
        loadingdialog.show();
        //getSharedPreferences("applevel", MODE_PRIVATE).getString("fcm_token","null")
        Ion.with(InsHomePage.this)
                .load(Constants.LOAD_AVAILABLE_REQUESTS)
                .setBodyParameter("fcm_token", "nothing")
                .setBodyParameter("filter", claimedJobs)
                // .setBodyParameter("ins_id", "8")
                .setBodyParameter("ins_id", insId)
                .asJsonArray().setCallback(new FutureCallback<JsonArray>() {
            @Override
            public void onCompleted(Exception e, JsonArray result) {
                Log.d("update_request", result.toString());
                if (loadingdialog.isShowing()) {
                    loadingdialog.dismiss();
                }
                if (!result.isJsonNull()) {
                    GsonBuilder mGsonBuilder = new GsonBuilder();
                    Gson mGson = mGsonBuilder.create();
                    parentList = Arrays.asList(mGson.fromJson(result, GsonIns[].class));
                    setAdapter(parentList);
                }
            }
        });
    }

    private void setAdapter(List<GsonIns> availableJobs) {

        TextView emptyMessage = (TextView) findViewById(R.id.emptyMessage);
        if (!availableJobs.isEmpty()) {
            emptyMessage.setVisibility(View.GONE);
            container.setVisibility(View.VISIBLE);
            insPackages = findViewById(R.id.ins_packages);
            insPackages.setLayoutManager(new LinearLayoutManager(InsHomePage.this, LinearLayoutManager.HORIZONTAL, false));
            mInsAdapter = new InsAdapter(availableJobs, claimedJobs, InsHomePage.this, requestsCounter);
            insPackages.setItemAnimator(new DefaultItemAnimator());
            insPackages.setAdapter(mInsAdapter);

            //click listner
            insPackages.addOnItemTouchListener(
                    new RecyclerItemClickListener(InsHomePage.this, insPackages, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Intent mIntent = new Intent(InsHomePage.this, CompleteInfo.class);
                            mIntent.putExtra("req_id", parentList.get(position).getReqId());
                            mIntent.putExtra("lear_id", parentList.get(position).getLearId());
                            mIntent.putExtra("lear_name", parentList.get(position).getLearName());
                            mIntent.putExtra("lear_email", parentList.get(position).getLearEmail());
                            mIntent.putExtra("lear_phone_number", parentList.get(position).getLearPhoneNumber());
                            mIntent.putExtra("lear_location", parentList.get(position).getLearLocation());
                            mIntent.putExtra("lear_postal_code", parentList.get(position).getLearPostalCode());
                            mIntent.putExtra("package_name", parentList.get(position).getPackageName());
                            mIntent.putExtra("package_hours", parentList.get(position).getPackageHours());
                            mIntent.putExtra("package_rate", parentList.get(position).getPackageRate());
                            mIntent.putExtra("lisence_type", parentList.get(position).getLisenceType());
                            mIntent.putExtra("date_to_start_course", parentList.get(position).getDateToStartCourse());
                            mIntent.putExtra("monday", parentList.get(position).getAvailMonday());
                            mIntent.putExtra("tuesday", parentList.get(position).getAvailTuesday());
                            mIntent.putExtra("wednesday", parentList.get(position).getAvailWednesday());
                            mIntent.putExtra("thursday", parentList.get(position).getAvailThursday());
                            mIntent.putExtra("friday", parentList.get(position).getAvailFriday());
                            mIntent.putExtra("saturday", parentList.get(position).getAvailSaturday());
                            mIntent.putExtra("sunday", parentList.get(position).getAvailSunday());
                            mIntent.putExtra("ins_id", insId);
                            mIntent.putExtra("claimed_jobs", claimedJobs);
                            startActivity(mIntent);
                        }

                        @Override
                        public void onLongItemClick(View view, int position) {

                        }
                    })
            );
        } else {
            Snackbar snackbar1 = Snackbar.make(container, "Nothing to show", Snackbar.LENGTH_SHORT);
            snackbar1.show();
            container.setVisibility(View.GONE);
            emptyMessage.setVisibility(View.VISIBLE);
        }
    }

    private List<GsonIns> prepareList(String condition) {
        Log.d("mylog", condition);
        childList = new LinkedList<>();
        // update recycler view list having lisence_type=manual
        if (condition.equals("manual")) {
            for (GsonIns listItem : parentList) {
                if (listItem.getLisenceType().equals(condition)) {
                    childList.add(listItem);
                }
            }
        }

        // update recycler view list having lisence_type=automatic
        if (condition.equals("automatic")) {
            for (GsonIns listItem : parentList) {
                if (listItem.getLisenceType().equals(condition)) {
                    childList.add(listItem);
                }
            }
        }

        if (condition.equals("5")) {
            for (GsonIns listItem : parentList) {
                if (listItem.getPackageHours().equals(condition)) {
                    childList.add(listItem);
                }
            }
        }

        if (condition.equals("10")) {
            for (GsonIns listItem : parentList) {
                if (listItem.getPackageHours().equals(condition)) {
                    childList.add(listItem);
                }
            }
        }

        if (condition.equals("20")) {
            for (GsonIns listItem : parentList) {
                if (listItem.getPackageHours().equals(condition)) {
                    childList.add(listItem);
                }
            }
        }

        if (condition.equals("30")) {
            for (GsonIns listItem : parentList) {
                if (listItem.getPackageHours().equals(condition)) {
                    childList.add(listItem);
                }
            }
        }

        Log.d("mylog", "Size of the list is " + childList.size());
        return childList;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.ins_drawer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // this will create right nav icon
        getMenuInflater().inflate(R.menu.ins_right_menu_indicator, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.ins_drawer);

        if (id == R.id.action_settings) {
            drawer.openDrawer(GravityCompat.END);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.ins_profile) {
            startActivity(new Intent(InsHomePage.this, InsSignUp.class).putExtra("update_instructor_profile", "yes"));
        } else if (id == R.id.ins_new_password) {
            startActivity(new Intent(InsHomePage.this, SetPassword.class).putExtra("ins_id", insId));
        } else if (id == R.id.ins_claimed_jobs) {
            claimedJobs = "yes";
            getMyAllPackages();
        } else if (id == R.id.ins_invite_friends) {

        } else if (id == R.id.ins_contact_us) {
            sendEmail();
        } else if (id == R.id.ins_logout) {
            logOut();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.ins_drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void sendEmail() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"muhammmadafzalsee@gmail.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Excell Driving");
        intent.setType("message/rfc822");
        startActivity(Intent.createChooser(intent, "Select an app to send email"));
        finish();
    }

    private void displayRightNavigation() {
        final NavigationView navigationViewRight = (NavigationView) findViewById(R.id.nav_view_right);
        navigationViewRight.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                // Handle navigation view item clicks here.
                int id = item.getItemId();

                if (id == R.id.ins_menu_manual) {
                    // Handle the camera action
                    setAdapter(prepareList("manual"));
                } else if (id == R.id.ins_menu_automatic) {
                    setAdapter(prepareList("automatic"));
                } else if (id == R.id.ins_menu_5hours) {
                    setAdapter(prepareList("5"));
                } else if (id == R.id.ins_menu_10hours) {
                    setAdapter(prepareList("10"));
                } else if (id == R.id.ins_menu_20hours) {
                    setAdapter(prepareList("20"));
                } else if (id == R.id.ins_menu_30hours) {
                    setAdapter(prepareList("30"));
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.ins_drawer);
                drawer.closeDrawer(GravityCompat.END);
                return true;
            }
        });
    }

    public void openDialogs() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_connection, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog connectionDialog = dialogBuilder.create();
        connectionDialog.show();
        LinearLayout reset_btn = (LinearLayout) dialogView.findViewById(R.id.ok);
        reset_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectionDialog.dismiss();
                finish();
            }
        });
    }

    public void logOut() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(InsHomePage.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_logout, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog findMeDialog = dialogBuilder.create();
        findMeDialog.show();
        TextView ok_btn = (TextView) dialogView.findViewById(R.id.ok);
        TextView cancel_btn = (TextView) dialogView.findViewById(R.id.cancel);

        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findMeDialog.dismiss();
                Utilities.getInstance(InsHomePage.this).clearAllSharedPreferences();
                Utilities.getInstance(InsHomePage.this).saveStringPreferences(Constants.SELECTED_USER_Type,"driver");
                startActivity(new Intent(InsHomePage.this, InsLogIn.class));
                finish();
            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findMeDialog.dismiss();
            }
        });
    }
}