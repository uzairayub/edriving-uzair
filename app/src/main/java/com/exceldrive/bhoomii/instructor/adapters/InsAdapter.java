package com.exceldrive.bhoomii.instructor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.GsonIns;
import java.util.List;

public class InsAdapter extends RecyclerView.Adapter<InsAdapter.MyViewHolder> {

    private List<GsonIns> packageList = null;
    private Context mContext;
    private String filter;
    private TextView requestsCounterText;

    public InsAdapter(List<GsonIns> packageList, String filter, Context mContext, TextView requestsCounterText) {
        this.requestsCounterText=requestsCounterText;
        this.packageList = packageList;
        this.mContext = mContext;
        this.filter = filter;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ins_item_packages, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        // Inflate the layout view you have created for the list rows here
        holder.pkgeName.setText(packageList.get(position).getPackageName());
        holder.pkgHours.setText(packageList.get(position).getPackageHours() + " Hours");
        holder.pkgLicenseType.setText(packageList.get(position).getLisenceType());
        holder.learName.setText(packageList.get(position).getLearName());
        holder.learPhoneNumber.setText(packageList.get(position).getLearPhoneNumber());
        holder.learPostalCode.setText(packageList.get(position).getLearPostalCode());
        holder.pkgPrice.setText("$" + packageList.get(position).getPackageRate());
        holder.jobId.setText("#" + packageList.get(position).getReqId());


        if (filter=="yes") {
            requestsCounterText.setText("Claimed Jobs "+(position+1)+"/"+packageList.size());
            holder.dualNatureTitle.setText("Claim on: ");
            holder.pkgLicenseType.setText(packageList.get(position).getReqAcceptedDate());
        } else {
            requestsCounterText.setText("Available requests "+(position+1)+"/"+packageList.size());
            holder.dualNatureTitle.setText("Lisence Type ");
            holder.pkgLicenseType.setText(packageList.get(position).getLisenceType());
        }
    }

    @Override
    public int getItemCount() {
        return packageList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView pkgeName, learName, learPhoneNumber, learPostalCode, pkgLicenseType, pkgHours, pkgPrice, counter, filterBarTitle, dualNatureTitle, jobId;

        public MyViewHolder(View view) {
            super(view);
            pkgLicenseType = (TextView) view.findViewById(R.id.pkg_license_type);
            learPostalCode = (TextView) view.findViewById(R.id.lear_postal_code);
            learName = (TextView) view.findViewById(R.id.lear_name);
            learPhoneNumber = (TextView) view.findViewById(R.id.lear_phone_number);
            pkgPrice = (TextView) view.findViewById(R.id.pkg_price);
            pkgHours = (TextView) view.findViewById(R.id.pkg_hours);
            pkgeName = (TextView) view.findViewById(R.id.pkgName);
            jobId = (TextView) view.findViewById(R.id.job_id);
//            counter = (TextView) view.findViewById(R.id.counter);
            dualNatureTitle = (TextView) view.findViewById(R.id.dual_purposes_title_lisence_type_and_accepted_date);
            filterBarTitle = (TextView) view.findViewById(R.id.filter_bar_title);
        }
    }
}