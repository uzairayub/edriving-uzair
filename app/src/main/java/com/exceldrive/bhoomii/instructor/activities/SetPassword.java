package com.exceldrive.bhoomii.instructor.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import static com.exceldrive.bhoomii.R.id.toolbar;

public class SetPassword extends AppCompatActivity {

    private EditText mOldPassword, mNewPassword, mConfirmPassword;
    private String token, oldPassword, newPassword, confirmNewPassword, passwordContainer, clickType = "check_pasword", insId;
    private Button changePassword;

    /*
    First user will enter the current password, if current password will be correct then the two fields will be showed to enter the for new password
    'clickType' --> Check the xml of this activity, that is containing 3 input fields, 1 for old password, 2 others for new & confirm password, this variable will use to set the visibility of these inpur fields from show to hide and vice versa
    'passwordContainer'--> there will be 2 times network call in this activity, 1 for to verify the old password $ second for to update new password into database, in each network call there we are sending a value of password, in first  case password will be the 'current passwrd' and in the second case the password will be 'new password' so passwordContainer will contain the either valaues
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);

        createToken();
        init();
    }


    private void createToken()
    {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( SetPassword.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();
            }
        });
    }

    private void init() {
        //get instructor id from home screen, as ins_id will use to update the password of a particular instructor on database
        insId = getIntent().getStringExtra("ins_id");
        mOldPassword = (EditText) findViewById(R.id.old_password);
        mNewPassword = (EditText) findViewById(R.id.new_password);
        mConfirmPassword = (EditText) findViewById(R.id.confirm_password);
        changePassword = (Button) findViewById(R.id.change_password);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });

        //toolbar
        Toolbar tool = (Toolbar) findViewById(toolbar);
        setSupportActionBar(tool);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void validation() {

        //When user will type his/her old password
        if (mOldPassword.getVisibility() == View.VISIBLE) {
            oldPassword = mOldPassword.getText().toString();
            if (!oldPassword.isEmpty()) {
                passwordContainer=oldPassword;
                checkAndSetPassword();
                clickType = "check_password";
            } else {
                mOldPassword.setError("Enter old password");
                mOldPassword.setFocusable(true);
            }
        }

        //when user will set his/her new password
        else if ((mNewPassword.getVisibility() == View.VISIBLE) || (mConfirmPassword.getVisibility() == View.VISIBLE)) {
            newPassword = mNewPassword.getText().toString();
            confirmNewPassword = mConfirmPassword.getText().toString();
            if (!newPassword.isEmpty()) {
                if (!confirmNewPassword.isEmpty()) {
                    if (newPassword.equals(confirmNewPassword)) {
                        clickType = "update_password";
                        passwordContainer=newPassword;
                        checkAndSetPassword();
                    } else {
                        Toast.makeText(this, "Password must be matched", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    mConfirmPassword.setError("Enter confirm password");
                    mConfirmPassword.setFocusable(true);
                    Toast.makeText(this, "new password", Toast.LENGTH_SHORT).show();
                }
            } else {
                mNewPassword.setError("Enter new password");
                mNewPassword.setFocusable(true);
                Toast.makeText(this, "confirm", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void checkAndSetPassword() {
        final ProgressDialog loadingdialog;
        loadingdialog = new ProgressDialog(SetPassword.this);
        loadingdialog.setTitle("Checking");
        loadingdialog.setMessage("Please wait..");
        loadingdialog.show();

        Ion.with(SetPassword.this)
                .load(Constants.UPDATE_PASSWORD)
                .setBodyParameter("fcm_token", token)
                .setBodyParameter("ins_password", passwordContainer)
                .setBodyParameter("ins_id", insId)
                .setBodyParameter("request_type", clickType)
                .asString().setCallback(new FutureCallback<String>()
        {
            @Override
            public void onCompleted(Exception e, String result) {
                if(e == null && result != null)
                {
                    Log.d("mylog", result);
                    if (loadingdialog.isShowing()) {
                        loadingdialog.dismiss();
                    }
                    if (clickType.equals("check_password")) {
                        if (result.equals("success")) {
                            mNewPassword.setVisibility(View.VISIBLE);
                            mConfirmPassword.setVisibility(View.VISIBLE);
                            mOldPassword.setVisibility(View.GONE);
                        } else {
                            Toast.makeText(SetPassword.this, "Incorrect password", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (result.equals("updated")) {
                            finish();
                            Toast.makeText(SetPassword.this, "Password has been set successfully", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SetPassword.this, "Fail", Toast.LENGTH_SHORT).show();
                        }
                    }
                    return;
                }
                Log.d("exception", e.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        finish();
        return true;
    }
}