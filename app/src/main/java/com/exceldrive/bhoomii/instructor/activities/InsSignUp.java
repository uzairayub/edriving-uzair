package com.exceldrive.bhoomii.instructor.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exceldrive.bhoomii.AppController;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.Util;
import com.exceldrive.bhoomii.Utils.Utilities;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class InsSignUp extends AppCompatActivity {

    private EditText mFirstName, mLastName, mEmail, mPassword, mPhoneNumber, mAddress, mPostalCode, mADINumber;
    private String token, firstName, lastName, emailAddress, password, phoneNumber, address, postalCode, ADINumber, insId="0",fcm_token, registerOrUpdate="register", progressDialogTitle="Registering" ;
    private LinearLayout mRegister, mUpdate;
    private TextView switchToLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructor_sign_up);
        createToken();
        init();
    }

    private void createToken()
    {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( InsSignUp.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();
            }
        });
    }

    private void init() {
        mFirstName = (EditText) findViewById(R.id.ins_first_name);
        mLastName = (EditText) findViewById(R.id.ins_last_name);
        mEmail = (EditText) findViewById(R.id.ins_email);
        mPassword = (EditText) findViewById(R.id.ins_password);
        mPhoneNumber = (EditText) findViewById(R.id.ins_phone_number);
        mAddress = (EditText) findViewById(R.id.ins_address);
        mPostalCode = (EditText) findViewById(R.id.ins_posta_code);
        mADINumber = (EditText) findViewById(R.id.ins_adi_number);
        mRegister = (LinearLayout) findViewById(R.id.ins_register);
        switchToLogin = (TextView) findViewById(R.id.switch_to_login_screen);


        //CheckPost: check either this activity open to register a new instructor or to update the profile of existing instructor
        if (getIntent().hasExtra("update_instructor_profile")) {
            switchToLogin.setVisibility(View.GONE);
            loadProfile();
            registerOrUpdate="update";
            progressDialogTitle="Updating";
            //we will not show password field as there is a separate option for it
            mPassword.setVisibility(View.GONE);

            //when this activity will use to open to shwo the complete detail of a claimed job then we will change the view in LinearLayout and remove the previous view,
            mRegister.setVisibility(View.GONE);
            mRegister = (LinearLayout) findViewById(R.id.ins_update);
            mRegister.setVisibility(View.VISIBLE);
        }

        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (AppController.haveNetworkConnection(InsSignUp.this))
                {
                    validation();
                }
                else
                {
                    openDialogs();
                }
            }
        });

        switchToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(InsSignUp.this, InsLogIn.class));
                overridePendingTransition(R.anim.incoming_screen_left_to_mean_position, R.anim.current_screen_move_mean_to_right);
                finish();
            }
        });
    }

    private void loadProfile() {
        SharedPreferences mSharedPreferences = getSharedPreferences("applevel", MODE_PRIVATE);
        firstName=mSharedPreferences.getString("ins_first_name","null");
        lastName=mSharedPreferences.getString("ins_last_name","null");
        emailAddress=mSharedPreferences.getString("ins_email","null");
        phoneNumber=mSharedPreferences.getString("ins_phone_number","null");
        address=mSharedPreferences.getString("ins_address","null");
        password=mSharedPreferences.getString("ins_password","null");
        postalCode=mSharedPreferences.getString("ins_zip_code","null");
        ADINumber=mSharedPreferences.getString("ins_adi_number","null");
        fcm_token=mSharedPreferences.getString("fcm_token",token);
        insId=mSharedPreferences.getString("ins_id","null");
        setText();
    }

    private void setText() {
        mFirstName.setText(firstName);
        mLastName.setText(lastName);
        mEmail.setText(emailAddress);
        mPhoneNumber.setText(phoneNumber);
        mAddress.setText(address);
        mPostalCode.setText(postalCode);
        mADINumber.setText(ADINumber);
    }

    private void validation() {

        //these values will be assigned to the respective variables in both cases (Registration, updateion). in updation same vlaues will be assigned that we will set above using loadProfile() method
        firstName = mFirstName.getText().toString();
        lastName = mLastName.getText().toString();
        emailAddress = mEmail.getText().toString();
        if (!registerOrUpdate.equals("update")) {
            password = mPassword.getText().toString();
        }

        phoneNumber = mPhoneNumber.getText().toString();
        address = mAddress.getText().toString();
        postalCode = mPostalCode.getText().toString();
        ADINumber = mADINumber.getText().toString();

        if (!firstName.isEmpty()) {
            if (!lastName.isEmpty()) {
                if (!emailAddress.isEmpty()&& Util.isValidEmail(emailAddress)) {
                    if (!password.isEmpty()) {
                        if (!address.isEmpty()) {
                            if (!phoneNumber.isEmpty()) {
                                if (!postalCode.isEmpty()) {
                                    if (!ADINumber.isEmpty()) {
                                        register();
                                    } else {
                                        mADINumber.setError("Enter ADI number");
                                        mADINumber.requestFocus();
                                    }
                                } else {
                                    mPostalCode.setError("Enter Postal Code");
                                    mPostalCode.requestFocus();
                                }
                            } else {
                                mPhoneNumber.setError("Enter Address");
                                mPhoneNumber.requestFocus();
                            }
                        } else {
                            mAddress.setError("Enter Phone Number");
                            mAddress.requestFocus();
                        }
                    } else {
                        mPassword.setError("Enter password");
                        mPassword.requestFocus();
                    }
                }
                else {
                    mEmail.setError("Enter Email Address");
                    mEmail.requestFocus();
                }
            } else {
                mLastName.setError("Enter Last Name");
                mLastName.requestFocus();
            }
        } else {
            mFirstName.setError("Enter First Name");
            mFirstName.requestFocus();
        }
    }

    private void register() {
        final ProgressDialog loadingdialog;
        loadingdialog = new ProgressDialog(InsSignUp.this);
        loadingdialog.setTitle(progressDialogTitle);
        loadingdialog.setMessage("Please wait..");
        loadingdialog.show();

        //Note: when this activity will use to register the Instructor then ins_id will be useless on web-API side, ins_id will only use when this activity will use to update the profile
        Ion.with(InsSignUp.this)
                .load(Constants.REGISTER_INSTRUCTOR)
                .setBodyParameter("ins_id", insId)
                .setBodyParameter("ins_name", firstName+" "+lastName)
                .setBodyParameter("ins_email", emailAddress)
                .setBodyParameter("ins_phone_number", phoneNumber)
                .setBodyParameter("ins_address", address)
                .setBodyParameter("ins_password", password)
                .setBodyParameter("ins_zip_code", postalCode)
                .setBodyParameter("ins_adi_number", ADINumber)
                .setBodyParameter("register_or_update", registerOrUpdate)
                .setBodyParameter("fcm_token", token)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {

                if(!result.equals(null)) {
                    Log.d("mylog32", result);
                    if (loadingdialog.isShowing()){
                        loadingdialog.dismiss();
                    }

                    if (result.equals("already exist")) {
                        Toast.makeText(InsSignUp.this, "User with this email already exists", Toast.LENGTH_SHORT).show();
                    } else if (result.equals("new email already exist")) {
                        Toast.makeText(InsSignUp.this, "You can't use this email as this is already registered! Try again", Toast.LENGTH_SHORT).show();
                    }
                    if (result.equals("updated")) {
                        Toast.makeText(InsSignUp.this, "Updated", Toast.LENGTH_SHORT).show();
                        addValuesToSharedPreference();
                        finish();
                    }
                    else
                    {
                        Utilities.getInstance(InsSignUp.this).saveBooleanPreferences(Constants.PREF_IS_INS_LOGIN, true);
                        insId=result;
                        SharedPreferences mSharedPreferences = getSharedPreferences("applevel", MODE_PRIVATE);
                        Editor mEditor = mSharedPreferences.edit();
                        mEditor.putString("is_registered", "yes");
                        mEditor.putString("ins_id", insId);
                        mEditor.commit();
                        addValuesToSharedPreference();
                        startActivity(new Intent(InsSignUp.this, InsRegisterSuccess.class));
                        finish();
                    }
                } else
                    Toast.makeText(InsSignUp.this, "Fail!, Try again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addValuesToSharedPreference(){
        SharedPreferences mSharedPreferences = getSharedPreferences("applevel", MODE_PRIVATE);
        Editor mEditor = mSharedPreferences.edit();
        mEditor.putString("ins_first_name", firstName );
        mEditor.putString("ins_last_name", lastName);
        mEditor.putString("ins_email", emailAddress);
        mEditor.putString("ins_password", password);
        mEditor.putString("ins_phone_number", phoneNumber);
        mEditor.putString("ins_address", address);
        mEditor.putString("ins_zip_code", postalCode);
        mEditor.putString("ins_adi_number", postalCode);
        mEditor.putString("ins_id", insId);
        mEditor.putString("is_registered", "yes");
        mEditor.commit();
    }

    public void openDialogs() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_connection, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog connectionDialog = dialogBuilder.create();
        connectionDialog.show();
        LinearLayout reset_btn = (LinearLayout) dialogView.findViewById(R.id.ok);
        reset_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectionDialog.dismiss();
                finish();
            }
        });
    }

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {

        if (false) {
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        finish();
        return true;
    }
}