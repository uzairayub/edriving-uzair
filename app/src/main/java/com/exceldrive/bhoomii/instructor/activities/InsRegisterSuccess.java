package com.exceldrive.bhoomii.instructor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.exceldrive.bhoomii.R;

public class InsRegisterSuccess extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ins_register_success);
        init();
    }

    private void init() {
        LinearLayout goToHome=(LinearLayout)findViewById(R.id.ins_continue);
        goToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(InsRegisterSuccess.this, InsHomePage.class));
                finish();
            }
        });
    }
}
