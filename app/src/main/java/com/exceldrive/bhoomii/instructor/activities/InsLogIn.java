package com.exceldrive.bhoomii.instructor.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.exceldrive.bhoomii.Activities.StartAs;
import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.Utilities;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class InsLogIn extends AppCompatActivity {

    private EditText mEmail, mPassword;
    private String email, password, firstName, lastName;
    private Button mLogin;
    private LinearLayout container;
    private TextView switchToSignup, switchToSelectionPage;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ins_login);
        createToken();
        init();
    }

    private void createToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( InsLogIn.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();
            }
        });
    }

    private void init() {
        mEmail = (EditText) findViewById(R.id.ins_email_address);
        mPassword = (EditText) findViewById(R.id.ins_password);
        mLogin = (Button) findViewById(R.id.login);
        container = (LinearLayout) findViewById(R.id.container);
        switchToSignup=(TextView)findViewById(R.id.switch_to_login);
        switchToSelectionPage=(TextView)findViewById(R.id.switch_to_selection_page);

        switchToSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(InsLogIn.this, InsSignUp.class));
                overridePendingTransition(R.anim.signin_incoming_screen_right_to_mean_position, R.anim.signin_current_screen_move_mean_to_left);
                finish();
            }
        });

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });
        switchToSelectionPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.getInstance(InsLogIn.this).clearAllSharedPreferences();
                startActivity(new Intent(InsLogIn.this, StartAs.class));
                overridePendingTransition(R.anim.incoming_screen_left_to_mean_position, R.anim.current_screen_move_mean_to_right);
                finish();
            }
        });
    }

    private void validation() {
        email = mEmail.getText().toString();
        password = mPassword.getText().toString();
        if (!email.isEmpty()) {
            if (!password.isEmpty()) {
                login();
            } else {
                mPassword.setError("Enter confirm password");
                mPassword.setFocusable(true);
            }
        } else {
            mEmail.setError("Enter new password");
            mEmail.setFocusable(true);
        }
    }

    private void login() {
        final ProgressDialog loadingdialog;
        loadingdialog = new ProgressDialog(InsLogIn.this);
        loadingdialog.setTitle("Logging");
        loadingdialog.setMessage("Please wait..");
        loadingdialog.show();

        Ion.with(InsLogIn.this)
                .load(Constants.LOGIN)
                .setBodyParameter("fcm_token", token)
                .setBodyParameter("ins_password", password)
                .setBodyParameter("ins_email", email)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                if (loadingdialog.isShowing()){
                    loadingdialog.dismiss();
                }
                Log.d("mylog", result);
                if (!result.equals("not exist")) {
                    try {
                        JSONArray array = new JSONArray(result);
                        JSONObject instance = array.getJSONObject(0);
                        addValuesToSharedPreference(instance.getString("ins_name"), instance);
                        Utilities.getInstance(InsLogIn.this).saveBooleanPreferences(Constants.PREF_IS_INS_LOGIN, true);
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                } else {
                    Snackbar snackbar1 = Snackbar.make(container, "User doesn't exist", Snackbar.LENGTH_SHORT);
                    snackbar1.show();

                }
            }
        });
    }

    private void addValuesToSharedPreference(String str, JSONObject learnerData) {
        //As instructor's full name is stored instead of first & last name . so that we will separate first name with last name, becuaase when user can edit hteir profile information afer logged in. by clicking on edit profile option
        String[] splited = str.split("\\s+");
        firstName = splited[0];
        lastName = splited[1];

        Log.d("mylog", firstName+" "+lastName);
        SharedPreferences mSharedPreferences = getSharedPreferences("applevel", MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putString("ins_first_name", firstName);
        mEditor.putString("ins_last_name", lastName);
        try {
            mEditor.putString("ins_email", learnerData.getString("ins_email"));
            mEditor.putString("ins_password", learnerData.getString("ins_password").toString());
            mEditor.putString("ins_phone_number", learnerData.getString("ins_phone_number").toString());
            mEditor.putString("ins_address", learnerData.getString("ins_address").toString());
            mEditor.putString("ins_zip_code", learnerData.getString("ins_postal_code").toString());
            mEditor.putString("ins_adi_number", learnerData.getString("ins_adi_number").toString());
            mEditor.putString("ins_id", learnerData.getString("ins_id").toString());
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        mEditor.putString("is_registered", "yes");
        mEditor.commit();
        startActivity(new Intent(InsLogIn.this, InsHomePage.class));
        finish();
    }

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {

        if (false) {
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }
}