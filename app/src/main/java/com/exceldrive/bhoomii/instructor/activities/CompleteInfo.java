package com.exceldrive.bhoomii.instructor.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.Utils.Constants;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.exceldrive.bhoomii.R.id.toolbar;

public class CompleteInfo extends AppCompatActivity {

    private String token, reqid, insId, learName, learEmail, learPhoneNumber, learPostalCode, learLocation, packageName, packageHours, packagePrice, lisenceType,dateToStartCourse, monday, tuesday, wednesday, thursday, friday, satuday, sunday, currentDate, claimedJobs = "no";
    private TextView mLearName, mLearEmail, mLearPhoneNumber, mLearPostalCode, mLearLocation, mPackageName, mPackageHours, mPackagePrice, mLisenceType, mDateToStartCourse, mMonday, mTuesday, mWednesday, mThursday, mFriday, mSaturday, mSunday;
    private LinearLayout proceedJob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_info);
        createToken();
        getIntentValues();
    }


    private void createToken()
    {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( CompleteInfo.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();
            }
        });
    }


    private void getIntentValues() {
        learName = getIntent().getStringExtra("lear_name");
        learEmail = getIntent().getStringExtra("lear_email");
        learPhoneNumber = getIntent().getStringExtra("lear_phone_number");
        learLocation = getIntent().getStringExtra("lear_location");
        learPostalCode = getIntent().getStringExtra("lear_postal_code");
        packageName = getIntent().getStringExtra("package_name");
        packageHours = getIntent().getStringExtra("package_hours");
        packagePrice = getIntent().getStringExtra("package_rate");
        lisenceType = getIntent().getStringExtra("lisence_type");
        dateToStartCourse = getIntent().getStringExtra("date_to_start_course");
        monday = getIntent().getStringExtra("monday");
        tuesday = getIntent().getStringExtra("tuesday");
        wednesday = getIntent().getStringExtra("wednesday");
        thursday = getIntent().getStringExtra("thursday");
        friday = getIntent().getStringExtra("friday");
        satuday = getIntent().getStringExtra("saturday");
        sunday = getIntent().getStringExtra("sunday");
        reqid = getIntent().getStringExtra("req_id");
        insId = getIntent().getStringExtra("ins_id");
        claimedJobs = getIntent().getStringExtra("claimed_jobs");
        init();
    }

    private void init() {
        Toolbar tool = (Toolbar) findViewById(toolbar);
        setSupportActionBar(tool);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mLearName = (TextView) findViewById(R.id.lear_name);
        mLearEmail = (TextView) findViewById(R.id.lear_email);
        mLearPhoneNumber = (TextView) findViewById(R.id.phone_number);
        mLearPostalCode = (TextView) findViewById(R.id.postal_code);
        mLearLocation = (TextView) findViewById(R.id.location);
        mPackageName = (TextView) findViewById(R.id.pkg_name);
        mPackageHours = (TextView) findViewById(R.id.pkg_hours);
        mPackagePrice = (TextView) findViewById(R.id.pkg_price);
        mLisenceType = (TextView) findViewById(R.id.lisence_type);
        mDateToStartCourse = (TextView) findViewById(R.id.date_to_start_course);
        mMonday = (TextView) findViewById(R.id.mon);
        mTuesday = (TextView) findViewById(R.id.tu);
        mWednesday = (TextView) findViewById(R.id.wed);
        mThursday = (TextView) findViewById(R.id.thu);
        mFriday = (TextView) findViewById(R.id.fri);
        mSaturday = (TextView) findViewById(R.id.sat);
        mSunday = (TextView) findViewById(R.id.sun);
        proceedJob = findViewById(R.id.proceed_job);
        if (claimedJobs.equals("no")) {
            proceedJob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openConfirmationDialog();
                }
            });
        } else {
            //when this activity will use to open to shwo the complete detail of a claimed job then we will change the view in LinearLayout and remove the previous view,
            proceedJob.removeAllViews();
            TextView disableText = new TextView(CompleteInfo.this);
            disableText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            disableText.setGravity(Gravity.CENTER);
            disableText.setText("Claimed!");
            disableText.setTextColor(Color.parseColor("#FFFFFF"));
            disableText.setTextSize(25);
            proceedJob.setBackgroundColor(getResources().getColor(R.color.theme_color3));
            proceedJob.addView(disableText);
        }
        setText();
    }


    private void setText() {
        mLearName.setText(learName);
        mLearEmail.setText(learEmail);
        mLearPhoneNumber.setText(learPhoneNumber);
        mLearPostalCode.setText(learPostalCode);
        mLearLocation.setText(learLocation);
        mPackageName.setText(packageName);
        mPackageHours.setText(packageHours);
        mPackagePrice.setText(packagePrice);
        mLisenceType.setText(lisenceType);
        mDateToStartCourse.setText(dateToStartCourse);
        mMonday.setText(monday);
        mTuesday.setText(tuesday);
        mWednesday.setText(wednesday);
        mThursday.setText(thursday);
        mFriday.setText(friday);
        mSaturday.setText(satuday);
        mSunday.setText(sunday);
    }

    //A dialog will be opened which ask to enter the code sent to the entered phone number
    public void openConfirmationDialog() {
        final AlertDialog confirmDialog;
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
        dialogBuilder.setView(dialogView);
        confirmDialog = dialogBuilder.create();
        confirmDialog.show();
        TextView confirm = (TextView) dialogView.findViewById(R.id.confirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialog.dismiss();
                updateRequest();
            }
        });
    }

    private void updateRequest() {
        getCurrentDate();
        final ProgressDialog loadingdialog;
        loadingdialog = new ProgressDialog(CompleteInfo.this);
        loadingdialog.setTitle("Updating request");
        loadingdialog.setMessage("Please wait..");
        loadingdialog.show();
        //getSharedPreferences("applevel", MODE_PRIVATE).getString("fcm_token","null")
        Ion.with(CompleteInfo.this)
                .load(Constants.UPDATE_REQUEST)
                .setBodyParameter("fcm_token", token)
                .setBodyParameter("req_id", reqid)
                .setBodyParameter("ins_id", insId)
                .setBodyParameter("req_accepted_date", currentDate)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                if (loadingdialog.isShowing()) {
                    loadingdialog.dismiss();
                }
                Log.d("mylog", result);
                Toast.makeText(CompleteInfo.this, "Done "+insId, Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        finish();
        return true;
    }

    private void getCurrentDate() {
        currentDate = new SimpleDateFormat("yyyy-MMM-dd").format(new Date());
    }
}