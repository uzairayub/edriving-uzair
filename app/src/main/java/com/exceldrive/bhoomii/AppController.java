package com.exceldrive.bhoomii;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDexApplication;

import com.exceldrive.bhoomii.NetWorkClient.ApiClient;
import com.exceldrive.bhoomii.NetWorkClient.ApiInterface;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;


public class AppController extends MultiDexApplication {
    private static AppController mInstance;
    private static ApiInterface apiService;
    private static DB snappy = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        configRetrofit();
    }

    private static void configRetrofit() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public static ApiInterface getApiService() {
        return apiService;
    }


    public static synchronized DB getSnappyInstance() {
        try {
            if (snappy == null) {
                snappy = DBFactory.open(mInstance);
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return snappy;
    }

    public static boolean haveNetworkConnection(Context mContext) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


}
