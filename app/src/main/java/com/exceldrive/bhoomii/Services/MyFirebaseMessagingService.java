package com.exceldrive.bhoomii.Services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.exceldrive.bhoomii.R;
import com.exceldrive.bhoomii.learner.Activities.LearnerMainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.exceldrive.bhoomii.Utils.Constants;
import com.exceldrive.bhoomii.Utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService
{

    @Override
    public void onNewToken(String token)
    {
        super.onNewToken(token);
        Log.d("","");
        Utilities.getInstance(getApplicationContext()).saveStringPreferences(Constants.PREF_FCM_TOKEN,token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getData().size() > 0)
        {
            Log.d("not_no_message", "Message data payload: " + remoteMessage.getData());
        }
        if (remoteMessage.getNotification() != null)
        {
            Log.d("not_null", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        try
        {
            JSONObject object = new JSONObject(remoteMessage.getData().get("sample"));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        sendNotification(this, remoteMessage.getData().get("message"));
    }

    public static void sendNotification(Context context, String messageBody) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        final String CHANNEL_ID = "CHANNEL_ID";
        if (Build.VERSION.SDK_INT >= 26)
        {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "CHANNEL_NAME", NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(mChannel);
        }
        Intent intent = new Intent(context, LearnerMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.mipmap.notification)
                .setColor(Color.parseColor("#2699FB"))  // small icon background color
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.notification))
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(pendingIntent);


        notificationBuilder.setContentText(messageBody);
        notificationManager.notify(0, notificationBuilder.build());
    }
}
